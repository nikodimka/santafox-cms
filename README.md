# Добро пожаловать в ветку разработки Santafox CMS #


### Как начать разрабатывать под Santafox CMS? ###

* [Ознакомитесь с документацией по разработке модулей ](https://goo.gl/nL8f4L)
* [Ознакомьтесь с рекомендуемым стилем программирования для Santafox CMS ](https://goo.gl/Xf4ZGv)
* [Документация доступна на сайте и на вики странице](https://goo.gl/pCwamt)

План тут: 
https://goo.gl/dspFU1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact