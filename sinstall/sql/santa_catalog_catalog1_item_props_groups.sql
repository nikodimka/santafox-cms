	DROP TABLE IF EXISTS `%PREFIX%_catalog_catalog1_item_props_groups`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_catalog_catalog1_item_props_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Товарные группы';