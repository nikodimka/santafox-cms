DROP TABLE IF EXISTS `%PREFIX%_page_fields`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_page_fields` (
  `id` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `required` tinyint(4) DEFAULT NULL,
  `default_value` varchar(500) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `values` varchar(100) DEFAULT NULL,
  `menu_show` tinyint(1) DEFAULT '1',
  `way_show` tinyint(1) DEFAULT '1',
  `admin_show` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Поля страницы';
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_page_fields` (`id`, `name`, `type`, `required`, `default_value`, `description`, `values`, `menu_show`, `way_show`, `admin_show`) VALUES
('caption', 'Название', 'string', 1, 'none', NULL, NULL, 1, 1, 0),
('name_title', 'Заголовок (title)', 'string', 1, NULL, NULL, NULL, 1, 1, 1),
('id_current_page', 'Уникальный ID', 'string', 1, NULL, NULL, NULL, 1, 1, 0),
('link_other_page', 'Переход на', 'pagesite', 1, NULL, NULL, NULL, 1, 1, 0),
('only_auth', 'Только авторизированым', 'checkbox', 1, '0', 'Если отмечен, доступен только авторизованным', NULL, 1, 1, 0),
('template', 'Шаблон', 'select', 1, NULL, NULL, '{"method":"function","call":"priv_templates_get"} ', 1, 1, 0);