DROP TABLE IF EXISTS `%PREFIX%_catalog_catalog1_cupons`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_catalog_catalog1_cupons` (
  `id` int(5) UNSIGNED NOT NULL,
  `textcode` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  `use_count` int(5) UNSIGNED DEFAULT '0',
  `type` enum('lowprice','dropprice') NOT NULL,
  `cupon_value` decimal(10,2) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;