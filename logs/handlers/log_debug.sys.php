<?php
/**
 * @author Ivan Kudryavsky [spam@mr-god.net]
 * @link http://master.virmandy.net
 * @link http://vk.com/id171080259
 *
 * Данный класс является обработчиком логов, отображающий логи на экране
 */
class Log_Debug extends LogHandler
{

    public function send($params,$host,$session,$level,$file,$line,$text,$id,$data)
    {
        if (!isset($_GET["debug"]) || $_GET["debug"] != $params["key"]) return;

        echo "<hr>";
        echo $level, (($id) ? " #" . $id : "");
        echo "<br>File: ",$file,":",$line;
        echo "<br><b>", $text,"</b>";
        if (count($data)>3){
            echo "<br>";
            foreach($data as $k=>$v){
                if(!in_array($k,array("ip","browser","url")))
                    echo $k,": ",$v,"<br>";
            }
        }
        echo "<br>";
    }

    public function get_name($lang)
    {
        return "Вывод лога на страницу";
    }

    public function get_description($lang)
    {
        return "Лог выводится на странице при передаче GET параметра debug=1 и при соответсвии ключа key. Например: index.html?debug=1&key=15sv2q5. При добавлении правила ключ необходимо указать в графе \"Параметры\"";
    }

    public function get_params($lang){
        return array(
            array("id"=>"key","name"=>"Ключ","description"=>"Значение ключа, при котом будет отображаться вывод на экран. Например: index.html?debug=15sv2q5, здесь ключ - 15sv2q5","required"=>true)
        );
    }
}