<?php
/**
 * @author Ivan Kudryavsky [spam@mr-god.net]
 * @link http://master.virmandy.net
 * @link http://vk.com/id171080259
 *
 * Данный класс является обработчиком логов, отсылающий логи на сервер лог-файлов www.graylog.org
 */
class Log_GrayLog extends LogHandler
{
    private $_server;
    private $_port;
    private $_maxChunkSize;
    private $_socket;

    function __construct(){
        $this->_maxChunkSize=8154;
    }

    public function send($params,$host,$session,$level,$file,$line,$text,$id,$data)
    {
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        if($level=="ERROR") $level=4;
        elseif($level=="WARNING") $level=3;
        elseif($level=="INFO") $level=2;
        elseif($level=="DEBUG") $level=1;
        $event=array(
            "version"=>"1.1",
            "short_message" => $text,
            "timestamp" => date("Y-m-d H:i:s.").$micro,
            "host" => $host,
            "level" => $level,
            "file" => $file,
            "line" => $line,
        );
        if($id!=null) $event["_id"]=$id;
        $event["_session"]=$session;
        if(is_array($data))
            foreach($data as $k=>$v){
                $event["_".$k]=$v;
            }

        //$message = gzcompress(json_encode($event));
        $message = json_encode($event,JSON_UNESCAPED_UNICODE);
        //echo $message . "<br>";
        if(!$this->_openSocket($params)){echo "Error Open Socket";return;}
        // Maximum size is 8192 byte. Split to chunks. (GELFv2 supports chunking)
        if (strlen($message) > $this->_maxChunkSize) {
            // Too big for one datagram. Send in chunks.
            $msgId = microtime(true) . rand(0,10000) . $this->_host;

            $parts = str_split($message, $this->_maxChunkSize);
            $i = 0;
            foreach($parts as $part) {
                if (@fwrite($this->_socket, $this->_prependChunkData($part, $msgId, $i, count($parts))) === false) {
                    //echo ('Aborting log. Could not write to socket');
                }else{
                    //echo 'SEND LOG<br>';
                }
                $i++;
            }

        } else {
            // Send in one datagram.
            if (@fwrite($this->_socket, $message) === false) {
                throw new Exception('Aborting log. Could not write to socket');
            }else{
                //echo '<br>'.$message.'<br>';
                //echo 'SEND LOG2<br>';
            }
        }
    }

    public function get_name($lang)
    {
        return "GrayLog";
    }

    public function get_description($lang)
    {
        return "Отправка лога на сервер лог-файлов <a href=\"//www.graylog.org\">GrayLog</a>";
    }

    public function get_params($lang){
        return array(
            array("id"=>"server","name"=>"Сервер","required"=>true),
            array("id"=>"port","name"=>"Порт","required"=>true,"default"=>"12201")
        );
    }

    private function _prependChunkData($data, $msgId, $seqNum, $seqCnt) {
        if (!is_string($data) || $data === '') {
            throw new Exception('Data must be a string and not be empty');
        }

        if (!is_integer($seqNum) || !is_integer($seqCnt) || $seqCnt <= 0) {
            throw new Exception('Sequence number and count must be integer. Sequence count must be bigger than 0.');
        }

        if ($seqNum > $seqCnt) {
            throw new Exception('Sequence number must be bigger than sequence count');
        }

        return pack('CC', 30, 15) . hash('sha256', $msgId, true) . pack('nn', $seqNum, $seqCnt) . $data;
    }

    private function _openSocket($params) {
        if ($params["server"] == $this->_server || $params["port"] == $this->_port)
            return true;
        $this->_server=$params["server"];
        $this->_port=$params["port"];
        $connection_string = 'udp://' . $this->_server . ':' . $this->_port;
        $this->_socket = stream_socket_client($connection_string);
        if(!$this->_socket) return false;
        return true;
    }
}