<?php
/**
 * @author Ivan Kudryavsky [spam@mr-god.net]
 * @link http://master.virmandy.net
 * @link http://vk.com/id171080259
 *
 * Данный класс является обработчиком логов, сохраняющий логи в файл
 */
class Log_File extends LogHandler
{

    public function send($params,$host,$session,$level,$file,$line,$text,$id,$data)
    {

        $log = date("d.m.Y H:i:s")." | ".$level." | ".$file.":".$line;
        if($id) $log.=" | #".$id;

        $log .="\nСессия: ".$session;
        $log .="\n";
        $log .= $text;
        $log .="\n";

        if (count($data)>0){
            foreach($data as $k=>$v){
                $log .= $k.":".$v."\n";
            }
        }

        $log .="\n";
        $abs_path = realpath(dirname(__FILE__)."/../../")."/";
        $file = $abs_path."logs/".$params["file"];
        if(file_exists($file) && filesize($file)>1048576){
            $filename = pathinfo($file,PATHINFO_FILENAME);
            $file = $abs_path."logs/".$filename."-".date("d.m.Y").".txt";
        }
        file_put_contents($file,$log,FILE_APPEND);
    }

    public function get_name($lang)
    {
        return "Сохранение лога в файл";
    }

    public function get_description($lang)
    {
        return "Лог сохраняется в файл в каталоге /logs/. При достижении размера файла в 1 Мб запись начинается в файл &lt;name&gt;-&lt;date&gt;.txt";
    }

    public function get_params($lang){
        return array(
            array("id"=>"file","name"=>"Название файла","required"=>true,"default"=>"log.txt")
        );
    }
}