<?php
/**
 * Пользовательские функции
 * Пользовательские функции предназначены для вызова функций в шаблоне с передачей параметров и возвратом результата.
 * Для вызова пользовательской функции в шаблоне необходимо прописать такую конструкцию [@function-<название функции>(<передаваемая ей строка>)]
 */

/**
 * Функция v_request($a) предназначена для вывода в шаблона значения из $_REQUEST.
 * Пример вызова [@function-v_request(page)]
 * Если вы параметр $_REQUEST[$name] отсутсвует вы можете вывести значение по умолчанию [@function-v_request(page ||| default_value)]
 * @param string $name
 * @param string $default
 * @return null|string
 */
function v_request($a){
    $a = explode(" ||| ", $a);
    $name = $a[0];
    if(isset($a[1])) $default = $a[1]; else $default = "";
    if(isset($_REQUEST[$name]))
        return $_REQUEST[$name];
    else
        return "".$default;
}

/**
 * Функция v_selected_print($a) предназначена для сохранения состояний выпадающих списков.
 * Пример вызова [@function-v_selected_print(name_select ;;||;; value)]
 * Данную функцию необходимо писать в каждом из <option> - <option value="55" [@function-v_selected_print(name_select ;;||;; 55)]>
 * Выводит ' selected="selected"' если $_REQUEST[$name]==$value
 * @param $name
 * @param $value
 * @return string
 */
function v_selected_print($a){
    $a = explode(" ;;||;; ",$a);
    $name = $a[0];
    if(isset($a[1])) $value = $a[1]; else  return '';
    if(isset($_REQUEST[$name]) && $_REQUEST[$name]==$value) return ' selected="selected"'; else return '';
}

/**
 * Функция v_checked_print($a) предназначена для сохранения состояния чекбоксов и радиокнопок.
 * Пример вызова [@function-v_checked_print(name_сhekbox)].
 * Например, <input type="checkbox" name="my_checkbox" [@function-v_checked_print(my_checkbox)]>
 * Выводит checked="checked" если  существует $_REQUEST[$name]
 * @param $name
 * @return string
 */
function v_checked_print($name){
    if(isset($_REQUEST[$name])) return ' checked="checked"'; else return '';
}

/**
 * Функция, выводящая на экран $text, если $value1 == $value2
 * Пример вызова [@function-v_if_equally(12  ;;||;; [@function-v_request(cid)] ;;||;; Текст, который отобразится)]
 * @param $a
 * @return string
 */
function v_if_equally($a){
    $a = explode(" ;;||;; ",$a);
    $value1 = $a[0];
    if(isset($a[1])) $value2 = $a[1]; else  return '';
    if(isset($a[2])) $text = $a[2]; else  return '';
    if($value1==$value2)
        return $text;
    else
        return "";
}

/**
 * Функция, выводящая на экран $text, если $value1 >= $value2
 * Пример вызова [@function-v_if_larger(5000;;||;;%price_value%;;||;;Скидка 50% на товары, цена которых от 5000 бубликов!])
 * @param $a
 * @return string
 */
function v_if_larger($a){
    $a = explode(" ;;||;; ",$a);
    $value1 = $a[0];
    if(isset($a[1])) $value2 = $a[1]; else  return '';
    if(isset($a[2])) $text = $a[2]; else  return '';
    if($value1>=$value2)
        return $text;
    else
        return "";
}

/**
 * Функция разделения пробелом числа на тысячи/миллиона и т.д. 
 * Пример вызова в каталоге цена товара [@function-price_space(%price_value%)] , результат 12000 --> 12 000
 * @param $money
 * @return string
 */
function price_space($money){ 
	$money = number_format($money,0,'',' '); 
	return $money;
}

/**
 * Функция создания канонической ссылки (галерея, новость, каталог товаров)
 * Ставится на метку в шаблон сайта <head></head> - @function-getCanonical()
 * Возможно придется заменить название таблиц на свои номера sf_newsi1 (2,3...) и catalog_catalog1 (2,3...)
 */
function getCanonical(){ 
    global $kernel;

    // для вопросов-ответов
    if ($kernel->pub_httpget_get("b")) {
        $qid = intval($kernel->pub_httpget_get("b"));
        $q_label = mysqli_fetch_assoc($kernel->runSQL("SELECT question FROM `".$kernel->pub_prefix_get()."_faq1_content` WHERE `id` = ".$qid));
        $q_label = $q_label["question"];
        return '<link rel="canonical" href="https://'.$kernel->pub_http_host_get().'/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($q_label).'-q'.$qid.'/">';
    }

    // для галереи
    if ($kernel->pub_httpget_get("gcat")) {
        $galid = intval($kernel->pub_httpget_get("gcat"));
        $gal_label = mysqli_fetch_assoc($kernel->runSQL("SELECT name FROM `".$kernel->pub_prefix_get()."_gallery_cats` WHERE `id` = ".$galid));
        $gal_label = $gal_label["name"];
        return '<link rel="canonical" href="https://'.$kernel->pub_http_host_get().'/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($gal_label).'-gcat'.$galid.'/">';
    }

    // для новостей
    if ($kernel->pub_httpget_get("id")) {
        $newsid = intval($kernel->pub_httpget_get("id"));
        $news_label = mysqli_fetch_assoc($kernel->runSQL("SELECT header FROM `".$kernel->pub_prefix_get()."_newsi1` WHERE `id` = ".$newsid));
        $news_label = $news_label["header"];
        return '<link rel="canonical" href="https://'.$kernel->pub_http_host_get().'/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($news_label).'-p'.$newsid.'.html">';
    }

    // для каталога
    if ($kernel->pub_httpget_get("itemid")) {
        $itemid = intval($kernel->pub_httpget_get("itemid"));
        $item_label = mysqli_fetch_assoc($kernel->runSQL("SELECT name FROM `".$kernel->pub_prefix_get()."_catalog_catalog1_items` WHERE `id` = ".$itemid));
        $item_label = $item_label["name"];
        $category_props = mysqli_fetch_assoc($kernel->runSQL("SELECT MIN(cat_id) AS cat_id FROM `".$kernel->pub_prefix_get()."_catalog_catalog1_item2cat` WHERE `item_id` = ".$itemid)); 
        $category_name = mysqli_fetch_assoc($kernel->runSQL("SELECT name FROM `".$kernel->pub_prefix_get()."_catalog_catalog1_cats` WHERE `id` = ".$category_props['cat_id']));
        return '<link rel="canonical" href="https://'.$kernel->pub_http_host_get().'/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category_name['name']).'-c'.$category_props['cat_id'].'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html">';
    } else {
        $catid = intval($kernel->pub_httpget_get("cid"));
        $category_name = mysqli_fetch_assoc($kernel->runSQL("SELECT name FROM `".$kernel->pub_prefix_get()."_catalog_catalog1_cats` WHERE `id` = ".$catid));
        if ($catid == 0) {
            return '<link rel="canonical" href="https://'.$kernel->pub_http_host_get().'/'.$kernel->pub_page_current_get().'/">';
        } else {
            return '<link rel="canonical" href="https://'.$kernel->pub_http_host_get().'/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category_name['name']).'-c'.$catid.'/">';
        }
    }

}