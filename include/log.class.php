<?php
/**
 * @author Ivan Kudryavsky [spam@mr-god.net]
 * @link http://master.virmandy.net
 * @link http://vk.com/id171080259
 */
class Log{
    private $handlers_txt = array();
    /** @var LogHandler[] */
    private $handlers = array();
    private $session = array();
    private $host;

    function __construct(){
        $this->host = HTTP_HOST;
        $this->session = md5($this->detectBrowser())."-".microtime(true);
        $this->connect_handler();
    }

    private function send_log($level, $text, $id=null, $data=array(), $file=-1, $line=-1){
        try {
            $handlers = $this->get_handlers($level, $id);
            if (count($handlers) == 0) return;
            $backtrace = debug_backtrace();
            if ($file === -1) $file = $backtrace[1]["file"];
            if ($line === -1) $line = $backtrace[1]["line"];
            $data["ip"] = Log::get_ip();
            if ($this->detectBrowser()) $data["browser"] = $this->detectBrowser();
            if (isset($_SERVER["REQUEST_URI"])) $data["url"] = $_SERVER["REQUEST_URI"];
            foreach ($handlers as $v) {
                if (isset($this->handlers[$v["handler"]]))
                    $this->handlers[$v["handler"]]->send(json_decode($v["params"], true), $this->host, $this->session, $level, $file, $line, $text, $id, $data);
            }
        } catch (Exception $e){
            echo "Error: ".$e->getFile().":".$e->getLine()."\n<br>";
            echo $e->getMessage()."\n<br>";
        }

    }
	
	public static function detectBrowser() {
		if(!empty($_SERVER['HTTP_USER_AGENT'])) {
			return strtolower($_SERVER['HTTP_USER_AGENT']);
		} else {
			return 'unrecognized';
		}
	}
	
    public static function error_handler($errno, $errstr , $errfile, $errline){
        global $Log;
        switch($errno){
            case 1: $level = "ERROR"; break;
            case 2: $level = "WARNING"; break;
            default: $level = "INFO";
        }
        if(SHOW_INT_ERRORE_MESSAGE){
            echo "<b>",$level,":</b> ",$errstr," in <b>",$errfile,"</b> on line <b>",$errline,"</b><br>\n";
        }
        $Log->send_log($level,$errstr,null,array(),$errfile,$errline);
        return true;
    }

    public function get_handlers($level,$id){
        global $kernel;
        $query = mysqli_query($kernel->db_link(),"SELECT * FROM ".PREFIX."_log_rules WHERE (`level` = '".$level."' OR `level` IS NULL) AND (`log_id` = '".$id."' OR `log_id` IS NULL)");
        $result = array();
        while($v = mysqli_fetch_assoc($query)){
            $result[] = array("handler"=>$v["handler"],"params"=>$v["params"]);
        }
        return $result;
    }
    public function get_handlers_all(){
        return $this->handlers;
    }
    public function get_handlers_id(){
        return $this->handlers_txt;
    }
    private function connect_handler(){
        $files = glob("./logs/handlers/*.php");
        foreach ($files as $file) if (!is_dir($file)) {
            /** @noinspection PhpIncludeInspection */
            include_once($file);
            $content=file_get_contents($file);
            if($content){
                preg_match_all('~class\s+([a-z0-9_]+)\s*extends\s*LogHandler~uis',$content,$m,PREG_SET_ORDER);
                foreach($m as $f) {
                    if (class_exists($f[1]) && !in_array($f[1], $this->handlers_txt)) {
                        /** @var $handler_class LogHandler */
                        $handler_class = new $f[1];
                        if (!$handler_class instanceof LogHandler)
                            continue;
                        $this->handlers_txt[] = $f[1];
                        $this->handlers[$f[1]] = $handler_class;
                    }
                }
            }
        }
    }

    function rules_add($level,$log_id,$handler,$params){
        global $kernel;
        $handlers = $this->get_handlers_id();
        if(!$handler || !in_array($handler,$handlers))    return "#incorrect_handler";
        if($level!="" && !in_array($level,array("ERROR","WARNING","INFO","DEBUG")))
            return "#incorrect_level";
        if(empty($log_id)) $log_id=null;
        if(empty($level)) $level=null;
        $handler_params = $this->get_handler_params($handler);
        foreach($handler_params as $v){
            if($v["required"]===true && (!isset($params[$v["id"]]) || empty($params[$v["id"]])))
                return "#invalid_params";
        }
        if(empty($data["params"])) $data["params"]=null;


        $data = array(
            "level" => $level,
            "log_id" => $log_id,
            "handler" => $handler,
            "params" => json_encode($params,JSON_UNESCAPED_UNICODE)
        );
        $kernel->db_add_record("_log_rules",$data);
        return true;
    }

    /**
     * Удаление правила обработки лога
     * @param $ids - массив id
     */
    public function rules_delete($ids){
        global $kernel;
        if(!is_array($ids)) return;
        foreach($ids as $k=>$v){
            $ids[$k] = intval($v);
        }
        $kernel->runSQL("DELETE FROM ".PREFIX."_log_rules WHERE id IN (".implode(",",$ids).")");
    }

    /** Функция возвращает параметры необходимые для указанного обработчика
     * @param $handler
     * @param $lang
     * @return mixed
     */
    public function get_handler_params($handler,$lang=false){
        if(isset($this->handlers[$handler]))
            return $this->handlers[$handler]->get_params($lang);
        else array();
    }

    public static function get_ip()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    static public function error($text, $id=null,$data=array(),$file=-1,$line=-1){
        global $Log;
        if(is_object($Log) && get_class($Log)=="Log")
            $Log->send_log("ERROR",$text,$id,$data,$file,$line);
    }
    static public function warning($text, $id=null,$data=array(),$file=-1,$line=-1){
        global $Log;
        if(is_object($Log) && get_class($Log)=="Log")
            $Log->send_log("WARNING",$text,$id,$data,$file,$line);
    }
    static public function info($text, $id=null,$data=array(),$file=-1,$line=-1){
        global $Log;
        if(is_object($Log) && get_class($Log)=="Log")
            $Log->send_log("INFO",$text,$id,$data,$file,$line);
    }
    static public function debug($text, $id=null,$data=array(),$file=-1,$line=-1){
        global $Log;
        if(is_object($Log) && get_class($Log)=="Log")
            $Log->send_log("DEBUG",$text,$id,$data,$file,$line);
    }
}

abstract class LogHandler{
    /** Даному методу будет передан лог
     * @param $params - Передает строку настройки из админ панели
     * @param $host - Хост
     * @param $session - Сессия
     * @param $level - Уровень лога
     * @param $file - Файл
     * @param $line - Строка
     * @param $text - Текст лога
     * @param $id - идентификатор лога
     * @param $data - дополнительные данные
     * @return mixed
     */
    abstract public function send($params,$host,$session,$level,$file,$line,$text,$id,$data);

    /** Метод возвращает название обработчика
     * @param $lang
     * @return mixed
     */
    abstract public function get_name($lang);

    /** Метод возвращает описание
     * @param $lang
     * @return mixed
     */
    abstract public function get_description($lang);

    /**
     * Функция, которая должна вернуть необходимые параметры для работы обработчика
     * @param $lang
     * @return mixed
     *
     * Доступные настройки параметров:
     * id - обязательное; латиница
     * name - обязательное; название, отображаемое пользователю
     * required - не обязательно;, если значение true - обозначает, что пользователь должен обязательно задать этот параметр
     * default - не обязательное; значение по умолчанию, подставляемое в поле ввода
     * Примеры ответов:
     * array(
            array("id"=>"file","name"=>"Название файла","required"=>true,"default"=>"log.txt"),
            array("id"=>"key","name"=>"Ключ","description"=>"Значение ключа, при котом будет отображаться вывод на экран. Например: index.html?debug=1&key=15sv2q5, здесь ключ - 15sv2q5","required"=>true)
       );
     */
    abstract public function get_params($lang);
}