<?php
class caching_out extends postprocessor
{
    public function do_postprocessing($s, $label)
    {
		if(isset($_SESSION['caching'][$label]))
			return base64_decode($_SESSION['caching'][$label]);
		else
			return $s;
    }

    public function get_name($lang)
    {
		if($lang=='EN')
			return "Output label cache";
		elseif($lang=='UA')
			return "Вивести кеш мітки";
		else
			return "Вывести кеш метки";
    }

    public function get_description($lang)
    {
		if($lang=='EN')
			return "Return cached labels";
		elseif($lang=='UA')
			return "Повернення кешованої мітки";
		else
			return "Возврат кешированной метки";
    }

}