<?php
/*
* Файл настроек безопасности.
* Внимание, изменение через конфигурационный файл нежелательно, рекомендуем вопсользоваться админкой
* В частности данные по изменению пароля и телефона - сделует делать только в админке
*/
$sec_config = array (
  'rules_in_admin' => 0,
  'xss_rules' => '#(<.*?(script|body|object|iframe|applet|meta|style|form|frameset|frame|svg).*?>)|(base64|data\\:|fromCharCode|expression|onmouse|onload|alert|getcookie|document\\.)#uim',
  'lfi_rules' => '#((\\.|%2e){2,}(\\/|%5c|\\\\)|config\\.php|ini\\.php)#uism',
  'php_rules' => '#(php:\\/\\/|(eval|preg_replace|require|include|call_user|create_func|array_filter|array_reduce|array_walk|array_map|reflection)\\()#uism',
  'sql_rules' => '#(UNION|SELECT|OUTFILE|ALTER|INSERT|DROP|TRUNCATE|(%tables%))\\s#uism',
  'tags_filter' => '<br><p><b><strong><i><ul><ol><li><table><tr><td><thead><tbody><dl><dd><dt>',
  'ignore_in_admin' => 0,
  'ignore_get' => '',
  'ignore_post' => '',
  'ignore_cookie' => '',
  'ignore_server' => 'TMP, PATH, SERVER_SOFTWARE, SERVER_ADDR, DOCUMENT_ROOT, SERVER_ADMIN',
  'block_options' => 
  array (
    'block_tor' => '0',
    'block_matrix' => 
    array (
      'get' => '1111',
      'post' => '1111',
      'cookie' => '1111',
      'server' => '1111',
    ),
  ),
  'ban_options' => 
  array (
    'ban_tor' => '0',
    'ban_subnet' => '1',
    'ban_forwarded_for' => '0',
    'ban_admin_enter' => '0',
    'ban_matrix' => 
    array (
      'get' => '0000',
      'post' => '0000',
      'cookie' => '0000',
      'server' => '0000',
    ),
  ),
  'notice_options' => 
  array (
    'notice_mail' => '0',
    'notice_sms' => '0',
    'notice_push' => '0',
    'notice_admin_enter' => '0',
    'notice_matrix' => 
    array (
      'get' => '0000',
      'post' => '0000',
      'cookie' => '0000',
      'server' => '0000',
    ),
  ),
  'login_options' => 
  array (
    'login_pass_crypt' => '0',
    'login_secret_key' => '',
    'login_sms' => '0',
    'login_captcha' => '0',
    'login_ip' => '',
  ),
  'other_options' => 
  array (
    'other_mail' => '',
    'other_sms_login' => '',
    'other_sms_pass' => '',
    'other_push_login' => '',
    'other_phone' => '',
  ),
);
?>