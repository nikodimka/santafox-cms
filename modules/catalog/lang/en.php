<?php
$type_langauge = 'en';

$il['catalog_show_generic_sql'] = 'Generic SQL';
$il['catalog_generic_sql_tpl'] = 'Template';
$il['catalog_generic_sql_query'] = 'SQL query';
$il['catalog_show_export'] = 'Export';

$il['catalog_basket_plus_zero_price_label'] = '+ negotiated price';
$il['catalog_add_variable_header_label'] = 'Adding a variable';
$il['catalog_edit_variable_header_label'] = 'Editing variable';

$il['catalog_menu_variables'] = 'Variables';
$il['catalog_variables_header_title'] = 'Variables';
$il['catalog_add_variable_label'] = 'Add Variable';
$il['catalog_list_variable_namedb_label'] = 'Identifier';
$il['catalog_list_variable_namefull_label'] = 'Name';
$il['catalog_list_variable_value_label'] = 'Value';
$il['catalog_list_variable_actions'] = 'Actions';
$il['catalog_variable_del_alert'] = 'Do you really want to delete the variable';

$il['catalog_variable_edit_namefull_label'] = 'Name';
$il['catalog_variable_edit_header'] = 'Editing variable';
$il['catalog_variable_edit_namedb_label'] = 'Identifier <br> <small> a-z, 0-9, _ </ small>';
$il['catalog_variable_edit_value_label'] = 'Value';
$il['catalog_variable_edit_save_label'] = 'Save';
$il['catalog_variable_save_msg_ok'] = 'Saved';
$il['catalog_variable_save_msg_incorrect_namedb'] = 'Invalid indentifier';
$il['catalog_variable_save_msg_empty_value'] = 'Not entered value';
$il['catalog_variable_save_msg_empty_name_full'] = 'Not entered the name';

$il['catalog_property_popular_days'] = 'Number of days by which statistics are taken into account when sorting by popularity';
$il['catalog_property_popular_days_descr'] = 'Sorting by popularity will work only when statistics are enabled and non-zero days count';
$il['catalog_property_date_format'] = 'Date format';
$il['catalog_property_way_item_tpl'] = 'Template for displaying the name of the goods on the road';
$il['catalog_property_way_cat_tpl'] = 'Template for displaying the category name on the road';

$il['catalog_property_basket_page'] = 'Output page for the goods basket';
$il['catalog_property_compare_page'] = 'Product comparison output page';
$il['catalog_property_order_page'] = 'Ordering page';
$il['catalog_property_catalog_page'] = 'Products list page';
$il['catalog_property_wishlist_page'] = 'User wishlist page';

/* Ajax response */
$il['catalog_property_basket_additem_success'] = 'Message when goods are added hastily';
$il['catalog_property_basket_additem_success_message'] = 'The product was hastily added!';
$il['catalog_property_basket_additem_error'] = 'Message when an item is added error';
$il['catalog_property_basket_additem_error_message'] = 'Error adding product to cart!';
$il['catalog_property_basket_upditem_success'] = 'Post when goods are updated hastily';
$il['catalog_property_basket_upditem_success_message'] = 'The basket was hastily updated!';
$il['catalog_property_basket_upditem_error'] = 'Message when product update failed';
$il['catalog_property_basket_upditem_error_message'] = 'Failed to update the Recycle Bin!';
$il['catalog_property_basket_removeitem_success'] = 'Post when goods are deleted hastily';
$il['catalog_property_basket_removeitem_success_message'] = 'The product was hastily deleted!';
$il['catalog_property_basket_removeitem_error'] = 'Message when an item was deleted';
$il['catalog_property_basket_removeitem_error_message'] = 'An error occurred while deleting the item!';
$il['catalog_property_basket_promocode_success'] = 'Message when using a promotional code hastily';
$il['catalog_property_basket_promocode_success_message'] = 'Promo code successfully applied!';
$il['catalog_property_basket_promocode_error'] = 'Message when the promotional code failed to apply';
$il['catalog_property_basket_promocode_error_message'] = 'Error applying promotional code!';
$il['catalog_property_basket_rempromocode_success'] = 'Message when the promotion code was unsuccessfully deleted';
$il['catalog_property_basket_rempromocode_success_message'] = 'Promo code successfully deleted!';
$il['catalog_property_basket_rempromocode_error'] = 'Message when the promotional code was deleted';
$il['catalog_property_basket_rempromocode_error_message'] = 'Error removing promotional code!';
$il['catalog_property_basket_orderreceived_success'] = 'Message when order is being hurriedly sent';
$il['catalog_property_basket_orderreceived_success_message'] = 'Order successfully registered!';
$il['catalog_property_basket_orderreceived_error'] = 'Message when an order was posted';
$il['catalog_property_basket_orderreceived_error_message'] = 'Error sending order!';
$il['catalog_property_compare_additem_success'] = 'Message when adding to the comparison list hastily';
$il['catalog_property_compare_additem_success_message'] = 'The product was successfully added to the comparison list';
$il['catalog_property_compare_additem_error'] = 'Error message when adding to the comparison list';
$il['catalog_property_compare_additem_error_message'] = 'Error adding item to the comparison list';
$il['catalog_property_wishlist_additem_success'] = 'Message when adding a wish list hastily';
$il['catalog_property_wishlist_additem_success_message'] = 'The product was successfully added to the wish list';
$il['catalog_property_wishlist_additem_error'] = 'Message when adding an error to the wish list';
$il['catalog_property_wishlist_additem_error_message'] = 'Error adding item to wishlist';
$il['catalog_property_wishlist_removeitem_success'] = 'Message when hastily deleting from the wish list';
$il['catalog_property_wishlist_removeitem_success_message'] = 'The product was successfully removed from the wishlist';
$il['catalog_property_wishlist_removeitem_error'] = 'Message when delete failed from wish list';
$il['catalog_property_wishlist_removeitem_error_message'] = 'Error removing item from wishlist';

$il['catalog_show_category'] = 'Display category card';
$il['catalog_category_details_tpl'] = 'Category Card Template (General)';
$il['catalog_category_details_tpl2'] = 'Template category card (product page)';
$il['catalog_show_wishlist'] = 'Display the wish list of the user';
$il['catalog_wishlist_list_tpl'] = 'User wish list template';
$il['catalog_wishlist_expiration'] = 'Number of days to store goods';
$il['catalog_wishlist_max_items'] =' Max. number of products in the list ';

$il['catalog_property_add_parents_cats'] = 'Parent category when adding an item?';
$il['catalog_property_add_parents_true'] = 'Mark Automatically';
$il['catalog_property_add_parents_false'] = 'Do not mark automatically';

$il['catalog_cat_header'] = 'List Header';
$il['catalog_cat_header_default'] = 'Product categories';
$il['catalog_items_cats_list_tpl'] = 'Category List Template';
$il['catalog_items_item_list_multi_group_tpl'] = 'Template for different groups';

$il['catalog_items_group_name_label'] = 'Product group';

$il['catalog_prop_in_group_header'] = 'Show me in the admin panel?';
$il['catalog_prop_in_filters_header'] = 'Show in filters?';
$il['catalog_edit_property_showinfilters'] = 'Add visibility to filters';
$il['catalog_show_outer_filter'] = 'Print external filter form';
$il['catalog_outer_filter_tpl'] = 'Search form template';
$il['catalog_outer_filter_header'] = 'Filter Block Header';
$il['catalog_outer_filter_header_default'] = 'Product Filters';
$il['catalog_outer_filter_counters'] = 'Counting the number of products?';
$il['catalog_outer_filter_counters_description'] = 'Warning! A large number of requests to the database. ';

$il['catalog_show_orders'] = 'Display User Orders';
$il['catalog_orders_list_header'] = 'Order list header';
$il['catalog_orders_list_header_default'] = 'List of your orders';
$il['catalog_orders_list_tpl_label'] = 'Order list template';
$il['catalog_orders_list_per_page_label'] = 'Orders per page';
$il['catalog_orders_list_maxpages_label'] = 'Max. pages in the block ';

$il['catalog_show_linked_items'] = 'Display related products';
$il['catalog_linked_items_tpl'] = 'List Template';
$il['catalog_edit_property_ismain'] = 'Basic (unique)';
$il['catalog_edit_property_visibility'] = 'Add visibility to all groups';
$il['catalog_add_linked_item_label'] = 'Add related product';
$il['catalog_no_main_prop_defined_label'] = 'The main property is not set';
$il['catalog_remove_linked_item_label'] = 'Remove from linked';
$il['catalog_linked_items_list_label'] = 'Related Products';
$il['catalog_linked_item_added_msg'] = 'Related product added';
$il['catalog_import_linked_col'] = '[Related Products]';
$il['catalog_import_linked_col_separator'] = 'Delimiter';
$il['catalog_import_bypass_1st_line_label'] = 'Skip the first line';
$il['catalog_items_header_label'] = 'List Header';
$il['catalog_show_other_items'] = 'Show other products of the category';
$il['catalog_other_items_tpl'] = 'List Template';
$il['catalog_other_per_page_label'] = 'Items per page';
$il['catalog_other_header_label'] = 'List Header';

$il['catalog_prop_sort_no'] = 'Not selected';
$il['catalog_prop_sort_asc'] = 'Ascending order';
$il['catalog_prop_sort_desc'] = 'Descending';

$il['catalog_group_default_cats_label'] = 'Default Categories';
$il['catalog_group_filter_cats_label'] = 'Categories in which to display filter options';
$il['catalog_items_all_list_search_results_mainlabel'] = 'Search Results';
// generate search form
$il['catalog_generate_search_form_label'] = 'Generate search form';
$il['catalog_generate_search_form_button_label'] = 'Generate';
$il['catalog_gen_search_form_enum_select_label'] = 'Select (drop-down menu, single value)';
$il['catalog_gen_search_form_enum_radio_label'] = 'Radio-buttons (one value)';
$il['catalog_gen_search_form_enum_checkboxes_label'] = 'Checkboxes (multiple values)';
$il['catalog_gen_search_form_string_accurate_label'] = 'Exact match (=)';
$il['catalog_gen_search_form_string_like_label'] = 'By occurrence (LIKE "% ...%")';
$il['catalog_gen_search_form_number_accurate_label'] = 'Exact match (=)';
$il['catalog_gen_search_form_number_diapazon_label'] = 'Range (> = and <=)';
$il['catalog_gen_search_form_ignore_field_label'] = 'Ignore';
$il['catalog_gen_search_form_file_select_label'] = 'Select (yes-no)';
$il['catalog_gen_search_form_file_checkbox_label'] = 'One checkbox';
$il['catalog_gen_search_form_file_radio_label'] = 'Radio-buttons (yes-no)';
$il['catalog_gen_search_form_file_necessary_label'] = 'Important';
$il['catalog_gen_search_form_file_notnecessary_label'] = 'It does not matter';

$il['catalog_gen_search_outfilename_label'] = '<b> The file name for the generated template </ b> <br/> <small> will be placed in / modules / catalog / templates_user / </ small>';
$il['catalog_gen_search_filtername_label'] = 'Name for the created filter';
$il['catalog_gen_search_filtertemplate_label'] = 'Output template for the created filter';
$il['catalog_edit_inner_filter_showsql_label'] = 'Show the generated SQL query';
$il['catalog_items_search_link_label'] = 'Search';

// for export
$il['catalog_export_csv_menuitem'] = 'Export to CSV';
$il['catalog_export_csv_formlabel'] = 'Export to CSV';
$il['catalog_export_csv_tpl_label'] = 'String Template';
$il['catalog_export_button_label'] = 'Export';
$il['catalog_export_groups_props_label'] = 'Properties';
$il['catalog_export_filter_select_label'] = 'Internal Filter';
$il['catalog_export_filter_select_all_items'] = '-out of the filter (all products) -';
$il['catalog_property_export_mime_type'] = 'Mime-type output';
$il['catalog_property_export_mime_standart'] = 'Standard';
$il['catalog_property_export_mime_xml'] = 'text/xml';
$il['catalog_property_export_mime_csv'] = 'text/csv';

// basket, orders, etc.
$il['catalog_show_order_label'] = 'Orders';
$il['catalog_show_order_fields'] = 'Basket fields (order) fields';
$il['catalog_basket_order_settings_label'] = 'Recycle Bin Settings';

// list of order fields
$il['catalog_order_prop_list_table_label_num'] = '#';
$il['catalog_order_prop_list_table_label_name'] = 'Name';
$il['catalog_order_prop_list_table_label_namedb'] = 'Identifier';
$il['catalog_order_prop_list_table_label_type'] = 'Type';
$il['catalog_order_prop_list_table_label_global'] = 'Required?';
$il['catalog_order_prop_list_table_label_actions'] = 'Actions';
$il['catalog_order_prop_list_table_label_order'] = 'Order';
$il['catalog_order_prop_list_table_label_change_sord'] = 'Save Order';
$il['catalog_order_prop_list_actions_del_alert'] = 'Do you really want to delete the property';
$il['catalog_edit_property_bt_add_field'] = 'Add Field';
$il['catalog_edit_is_required_label'] = 'Required?';
$il['catalog_order_field_regexp_label'] = 'Regexp';
$il['catalog_order_field_saved_msg'] = 'Saved';
$il['catalog_order_templates_header'] = 'Templates';
$il['catalog_order_template_basket_items_label'] = 'Template for cart products';
$il['catalog_order_template_order_label'] = 'Order Template Template';
$il['catalog_order_action_regenerate_tpls_label'] = 'Generate';
$il['catalog_order_tpls_regenerated_msg'] = 'Templates are generated';
$il['catalog_order_tpls_regenerated_translit_msg'] = 'Templates are generated with translit names';
$il['catalog_order_show_order_form'] = 'Display Order Form';
$il['catalog_order_manager_mail_tpl'] = 'Template letter to the manager';
$il['catalog_order_manager_mail_subj'] = 'The subject of the letter to the manager';
$il['catalog_order_manager_email'] = 'Email Manager';
$il['catalog_order_user_mail_tpl'] = 'Template of the letter to the user';
$il['catalog_order_user_mail_subj'] = 'Subject of the message to the user';
$il['catalog_edit_order_field_label_add'] = 'Adding a field to the order';
$il['catalog_edit_order_field_label_edit'] = 'Edit Order Field';

// coupons and discounts
$il['catalog_sshow_cupons_label'] = 'Coupons and discounts';
$il['catalog_cupons_header_title'] = 'Coupon List';
$il['catalog_cupons_table_empty'] = 'The list is empty';
$il['catalog_add_cupon_label'] = 'Add a new coupon';

$il['catalog_new_cupon_header_label'] = 'New coupon';
$il['catalog_edit_cupon_header_label'] = 'Edit Coupon';
$il['catalog_list_cupon_action_edit'] = 'Edit Coupon';
$il['catalog_cupon_del_alert'] = 'Are you sure you want to delete the coupon !?';
$il['catalog_cupon_edit_save_label'] = 'Save';
$il['catalog_cupon_id'] = 'ID coupon';
$il['catalog_cupon_textcode'] = 'Coupon Code';
$il['catalog_cupon_textcode_decr'] = 'Unique, in the A-z format, 0-9, -_';
$il['catalog_cupon_created'] = 'Date of creation';
$il['catalog_cupon_expiration'] = 'Expires';
$il['catalog_cupon_use_count'] = 'Number of Uses';
$il['catalog_cupon_type'] = 'Discount type';
$il['catalog_cupon_cupon_value'] = 'Discount value';
$il['catalog_cupon_type_lowprice'] = 'at a fixed value';
$il['catalog_cupon_type_dropprice'] = 'per percentage';

$il['catalog_cupon_save_msg_incorrect_textcode'] = 'Error in coupon code format';
$il['catalog_cupon_save_msg_empty_textcode'] = 'Error! The coupon code can not be empty ';
$il['catalog_cupon_save_msg_incorrect_use_count'] = 'Error in usage count format';
$il['catalog_cupon_save_msg_empty_use_count'] = 'Error! Number of coupon uses can not be empty ';
$il['catalog_cupon_save_msg_incorrect_value'] = 'Error in the format of the discount value';
$il['catalog_cupon_save_msg_empty_value'] = 'Error! The value of the discount can not be empty ';
$il['catalog_cupon_save_msg_empty_expiration'] = 'Error! Expiration of the coupon can not be empty ';
$il['catalog_cupon_save_msg_incorrect_expiration'] = 'Error! The expiration date of the coupon is less than the current date ';
$il['catalog_cupon_save_msg_ok'] = 'Saved!';
$il['catalog_cupon_save_msg_error'] = 'Error while saving!';
$il['catalog_cupon_generate_label'] = 'Generate';

// for actions
$il['catalog_common_props'] = 'General Properties';
$il['catalog_show_item_name'] = 'Generate the name of the element';
$il['catalog_show_inner_selection_results'] = 'Form sample by internal filter';
$il['catalog_inner_selection_filter_label'] = 'Internal Filter';
$il['catalog_show_item_fields_label'] = 'Template for displaying the name of the email';
$il['catalog_show_item_fields_default_label'] = 'Default email name';
$il['catalog_show_cat_fields_label'] = 'Template for outputting the category of el-ta';
$il['catalog_show_cat_fields_default_label'] = 'Name of the category by default';


$il['catalog_show_basket_label'] = 'Displays the basket sticker';
$il['catalog_show_basket_items'] = 'Display the contents of the basket';
$il['catalog_basket_label_empty_tpl'] = 'Sticker Template for Empty Recycle Bin';
$il['catalog_basket_label_notempty_tpl'] = 'Sticker Template for the Recycle Bin';
$il['catalog_basket_items_tpl'] = 'Template of the goods basket';

$il['catalog_inner_filter_test'] = 'Filter test';

$il['catalog_inner_filters'] = 'Internal filters';
$il['catalog_list_inner_filter_actions'] = 'Actions';
$il['catalog_list_inner_filter_stringid_label'] = 'ID';
$il['catalog_list_inner_filter_name_label'] = 'Name';
$il['catalog_add_inner_filter_label'] = 'Add internal filter';
$il['catalog_inner_filter_actions_del_alert'] = 'Do you really want to delete the internal filter';
$il['catalog_edit_inner_filter_name_label'] = 'Name';
$il['catalog_edit_inner_filter_stringid_label'] = 'String ID';
$il['catalog_edit_inner_filter_query_label'] = 'Request';
$il['catalog_edit_inner_filter_template_label'] = 'Template';
$il['catalog_edit_inner_filter_limit_label'] = 'Limit';
$il['catalog_edit_inner_filter_perpage_label'] = 'Items per page';
$il['catalog_edit_inner_filter_maxpages_label'] = 'Max. number of pages in the block ';
$il['catalog_edit_inner_filter_groupid_label'] = 'Filter product group';
$il['catalog_edit_inner_filter_targetpage_label'] = 'Product Page';
$il['catalog_edit_inner_filter_categories_label'] = 'Categories';
$il['catalog_edit_inner_filter_label_add'] = 'Adding an internal filter';
$il['catalog_edit_inner_filter_label_edit'] = 'Edit internal filter';
$il['catalog_edit_inner_filter_action_save'] = 'Save Filter';
$il['catalog_edit_inner_filter_all_categories_label'] = 'All categories';
$il['catalog_edit_inner_filter_current_category_label'] = 'Current category';
$il['catalog_select_categories_label'] = 'Select Categories';
$il['catalog_edit_inner_filter_save_msg_ok'] = 'Internal filter saved';
$il['catalog_edit_inner_filter_save_msg_error'] = 'Error while saving internal filter';
$il['catalog_edit_inner_filter_save_msg_emptyfields'] = 'Required fields are missing';
$il['catalog_edit_inner_filter_save_msg_stringid_exists'] = 'String ID already exists';
$il['catalog_filter_all_groups'] = 'All groups (common properties)';
$il['catalog_filter_detect_group'] = 'Determine automatically';

$il['catalog_items_page'] = 'Page to display the list of products';
// for import
$il['catalog_import_csv_successfull_msg'] = 'Import Complete';
$il['catalog_import_csv_menuitem'] = 'Import from CSV';
$il['catalog_import_csv_formlabel'] = 'Import from CSV';
$il['catalog_import_file_label'] = 'File';
$il['catalog_import_textarea_label'] = 'Clipboard';
$il['catalog_import_separator_label'] = 'Delimiter';
$il['catalog_import_separator_tab'] = 'Tabulation';
$il['catalog_import_separator_comma'] = 'Comma';
$il['catalog_import_separator_semicolon'] = 'Semicolon';
$il['catalog_import_ignore_column_label'] = 'Ignore';
$il['catalog_import2group_label'] = 'In com. group ';
$il['catalog_import2cat_label'] = 'To Category';
$il['catalog_new_import2cat_label'] = 'Category for new (optional)';
$il['catalog_cat_not_selected_label'] = 'not selected';
$il['catalog_import_label'] = 'Import';
$il['catalog_import_uniq_field_label'] = 'Unique field';

$il['catalog_base_name'] = 'Product Catalog';
$il['catalog_modul_base_name1'] = 'Product Catalog';

$il['catalog_property_items_per_page_admin'] = 'Elements per page in AI';

$il['catalog_show_cats'] = 'Generate list of categories';
$il['catalog_show_items'] = 'Generate a list of products';
$il['catalog_tpl'] = 'Template';
$il['catalog_items_list_tpl'] = 'Product List Template';

$il['catalog_cats_list_tpl'] = 'Category List Template';

$il['catalog_items_per_page_label'] = 'Items per page';
$il['catalog_show_cats_if_empty_items_label'] = 'Do I list categories if there are no products?';

// Types of available fields, catalog_prop_type _ ??? , where ??? property identifier.
$il['catalog_prop_type_text'] = 'Text';
$il['catalog_prop_type_string'] = 'String';
$il['catalog_prop_type_html'] = 'HTML';
$il['catalog_prop_type_date'] = 'Date-time';
$il['catalog_prop_type_file'] = 'File';
$il['catalog_prop_type_pict'] = 'Image';
$il['catalog_prop_type_number'] = 'Numeric';
$il['catalog_prop_type_enum'] = 'Set of values (ENUM)';
$il['catalog_prop_type_enum_notselect'] = 'Not selected';
$il['catalog_prop_type_set'] = 'Set of values (SET)';
$il['catalog_prop_type_fileselect'] = 'Select file (on server)';
$il['catalog_prop_type_imageselect'] = 'Select image (on server)';

//Menu
$il['catalog_menu_label'] = 'Directory Configuration';
$il['catalog_menu_all_props'] = 'General properties of groups';
$il['catalog_menu_label_cats'] = 'Categories';
$il['catalog_menu_groups'] = 'Product groups of their properties';
$il['catalog_menu_items'] = 'Products';
$il['catalog_menu_cat_props'] = 'Category Properties';
$il['catalog_menu_label_import_export'] = 'Import-export';

// Property Group List
$il['catalog_add_group_label'] = 'Create a new group';
$il['catalog_list_group_number_label'] = 'No.';
$il['catalog_list_group_name_label'] = 'Group name';
$il['catalog_list_group_id_label'] = 'Identifier';
$il['catalog_list_group_id_action'] = 'Actions';
$il['catalog_list_group_action_edit'] = 'Edit';
$il['catalog_list_group_action_add_item'] = 'Add item';
$il['catalog_list_group_action_add_prop'] = 'Properties';
$il['catalog_list_group_action_create_amin_template'] = 'Create admin templates';
$il['catalog_list_group_action_create_amin_template_alert'] = 'If a template for this product group already exists, it will be overwritten';

// Edit / add new group form
$il['catalog_edit_group_label_edit'] = 'Editing a Product Group';
$il['catalog_edit_group_label_add'] = 'Parameters of the new commodity group';
$il['catalog_edit_group_name_label'] = 'Group name';
$il['catalog_edit_group_id_label'] = 'Group ID (Latin)';
$il['catalog_edit_group_template_list_label'] = 'Product List Template';
$il['catalog_edit_group_template_item_label'] = 'Product Card Template';
$il['catalog_edit_group_action_save'] = 'Save';
$il['catalog_edit_group_save_msg_ok'] = 'Product group saved';
$il['catalog_edit_group_save_msg_err'] = 'Error while saving';
$il['catalog_edit_group_action_new_template'] = 'Create templates';
$il['catalog_edit_group_action_new_template_alert'] = 'New templates for this product group will be created. <br> ATTENTION! Existing templates will be overwritten and all changes to them will be lost. Continue?';
// List of properties for the group
$il['catalog_group_prop_list_mainlabel'] = 'List of commodity group properties';
$il['catalog_group_prop_list_mainlabel_global'] = 'List of common properties';

$il['catalog_group_prop_list_table_label_num'] = 'No.';
$il['catalog_group_prop_list_table_label_name'] = 'Name';
$il['catalog_group_prop_list_table_label_namedb'] = 'Identifier';
$il['catalog_group_prop_list_table_label_type'] = 'Type';
$il['catalog_group_prop_list_table_label_global'] = 'General';
$il['catalog_group_prop_list_table_label_actions'] = 'Actions';
$il['catalog_group_prop_list_table_label_order'] = 'Order';
$il['catalog_group_prop_list_table_label_change_sord'] = 'Save order and visibility';
$il['catalog_group_prop_list_actions_del_alert'] = 'Do you really want to delete the property';

// Edit / add property form
$il['catalog_edit_property_label_globalprop'] = 'General Property Parameters';
$il['catalog_edit_property_label_group'] = 'Parameters of the group property';
$il['catalog_edit_property_label_globalprop_add'] = 'New Common Property';
$il['catalog_edit_property_label_group_add'] = 'New Group Properties';
$il['catalog_edit_property_name'] = 'Name';
$il['catalog_edit_property_namedb'] = 'Identifier';
$il['catalog_edit_property_sorted'] = 'Sorting / selection';
$il['catalog_edit_property_type'] = 'Property type';
$il['catalog_edit_property_show_in_list'] = 'Display administrative interface in the product list';
$il['catalog_edit_property_type'] = 'Property type';
$il['catalog_edit_property_bt_save'] = 'Save';
$il['catalog_edit_property_bt_add_prop'] = 'Add property';
$il['catalog_edit_property_added_msg'] = 'Property added';
$il['catalog_edit_property_saved_msg'] = 'Property was saved';
$il['catalog_edit_property_enum_label'] = 'Possible Values';
$il['catalog_edit_property_enum_help'] = '(each value starts with a new line)';
$il['catalog_edit_property_enum_val_add_label'] = 'New value in the set';
$il['catalog_edit_property_enum_val_add_bt'] = 'Add';
$il['catalog_edit_property_enum_del_alert'] = 'Do you really want to delete the value';
$il['catalog_edit_property_enum_add_msg'] = 'New value added';

$il['catalog_edit_property_pict_path'] = 'Image save path';

// Working with a list of properties for categories
$il['catalog_edit_property_cat_mainlabel'] = 'Properties of all catalog categories';
$il['catalog_edit_property_cat_num_label'] = 'No.';
$il['catalog_edit_property_cat_name_label'] = 'Name';
$il['catalog_edit_property_cat_dbname_label'] = 'Identifier';
$il['catalog_edit_property_cat_type_label'] = 'Property type';
$il['catalog_edit_property_cat_action_label'] = 'Actions';
$il['catalog_edit_property_cat_action_alert_del'] = 'You really want to delete the property';
$il['catalog_edit_property_cat_add_prop_label'] = 'Add property to category';
$il['catalog_edit_property_cat_add_name_label'] = 'Name';
$il['catalog_edit_property_cat_add_dbname_label'] = 'Identifier';
$il['catalog_edit_property_cat_add_type_label'] = 'Property type';
$il['catalog_edit_property_cat_add_enum_label'] = 'Possible Values';
$il['catalog_edit_property_cat_add_enum_help'] = '(each value starts with a new line)';
$il['catalog_edit_property_cat_add_new_prop'] = 'Add property';
$il['catalog_edit_property_cat_add_new_prop_msg'] = 'New property added';
$il['catalog_edit_property_cat_edit_label_main'] = 'Edit Category Property';
$il['catalog_edit_property_cat_edit_name_label'] = 'Name';
$il['catalog_edit_property_cat_edit_dbname_label'] = 'Identifier';
$il['catalog_edit_property_cat_edit_type_label'] = 'Property Type';
$il['catalog_edit_property_cat_edit_bt_save'] = 'Save';
$il['catalog_edit_property_cat_enum_label'] = 'Possible Values';
$il['catalog_edit_property_cat_enum_addnew'] = 'Add value';
$il['catalog_edit_property_cat_enum_addnew_bt'] = 'Add';
$il['catalog_edit_property_cat_enum_addnew_msg'] = 'New property value added';
$il['catalog_edit_property_cat_enum_del_alert'] = 'Do you really want to delete the value from the set';

// Edit the parameters of the category
$il['catalog_edit_category_label_main'] = 'Edit Category';
$il['catalog_edit_category_isdefault_label'] = 'Default category';
$il['catalog_edit_category_need_select_label'] = 'Value not selected';
$il['catalog_edit_category_bt_save_label'] = 'Save';
$il['catalog_edit_category_prop_file_del_alert'] = 'Do you really want to delete the downloaded file';

// List of all products
$il['catalog_items_all_list_filter_mainlabel'] = 'List of all store items';
$il['catalog_items_all_list_filter_items_group'] = 'Show products';
$il['catalog_items_all_list_filter_all_group'] = 'members of all groups';
$il['catalog_items_all_list_filter_no_cat'] = 'without category';
$il['catalog_items_all_list_table_num'] = 'No.';
$il['catalog_items_all_list_table_action'] = 'Actions';
$il['catalog_items_all_list_table_action_del_sel'] = 'Delete selected';
$il['catalog_items_all_list_table_action_no'] = 'Not selected';
$il['catalog_items_all_list_table_action_with_sel'] = 'Actions with checked';
$il['catalog_items_all_list_table_action_with_sel_go'] = 'Run';
$il['catalog_items_all_list_table_action_check_all'] = 'Mark all';
$il['catalog_items_all_list_table_action_uncheck_all'] = 'Uncheck All';

$il['catalog_items_all_list_table_group'] = 'Belongs to the group';
$il['catalog_items_all_list_table_action_alertdel'] = 'Are you sure you want to delete the item?';
$il['catalog_items_all_list_table_action_edit'] = 'Edit Item';
$il['catalog_items_all_list_nogroups_msg'] = 'You need at least one product group to add the product';
$il['catalog_items_all_list_add_item_label'] = 'New product of the group';
$il['catalog_items_all_list_add_item_bt_label'] = 'Add';
$il['catalog_items_all_list_no_items'] = 'There are no products in the selected group';
$il['catalog_items_all_list_table_checkbox'] = 'Batch processing';
$il['catalog_items_all_list_table_label_save_fields'] = 'Save Fields';

// Category Contents
$il['catalog_list_category_edit_label'] = 'Edit Category';
$il['catalog_list_category_additem_label'] = 'Add item';
$il['catalog_list_category_add_item_label'] = 'New product of the group';
$il['catalog_list_category_add_item_bt_label'] = 'Add';
$il['catalog_list_category_item_list_label'] = 'Products of this category';
$il['catalog_list_category_item_list_notitems'] = 'There are no products in the category';
$il['catalog_list_category_item_action_edit'] = 'Edit Item';
$il['catalog_list_category_not_select_cat'] = 'The category contains nothing';
$il['catalog_list_category_item_table_num'] = 'Batch processing';
$il['catalog_list_category_item_table_action'] = 'Actions';
$il['catalog_list_category_item_table_order_num'] = 'Sorting order';
$il['catalog_list_category_item_table_check_all'] = 'Mark all';
$il['catalog_list_category_item_table_uncheck_all'] = 'Uncheck All';
$il['catalog_list_category_item_table_action_selected'] = 'Actions with checked:';
$il['catalog_list_category_item_table_action_selected0'] = 'Not selected';
$il['catalog_list_category_item_table_action_selected1'] = 'Exclude from current category';
$il['catalog_list_category_item_table_action_selected2'] = 'Move to Category';
$il['catalog_list_category_item_table_action_selected3'] = 'DELETE';
$il['catalog_list_category_item_table_action_run'] = 'Execute';
$il['catalog_list_category_item_table_action_change_sord'] = 'Edit';
$il['catalog_list_category_item_table_alertdel'] = 'Do you really want to completely destroy the product?';
$il['catalog_list_category_item_table_alertdel_alt'] = 'Destroy the goods';

// Item editing form
$il['catalog_item_edit_mainlabel'] = 'Editing product properties';
$il['catalog_item_edit_table_nameprop_label'] = 'Property name';
$il['catalog_item_edit_table_value_label'] = 'Property Value';
$il['catalog_item_edit_available_label'] = 'Display product in the store';
$il['catalog_item_edit_insitemap_label'] = 'Display product in site map';
$il['catalog_item_edit_file_del_alert'] = 'Do you really want to delete the downloaded file';
$il['catalog_item_edit_bt_save_label'] = 'Save Changes';
$il['catalog_item_edit_incategories_label'] = 'Included in categories:';
$il['catalog_item_edit_pict_add_mark_for_big'] = 'Watermark to the BIG image';
$il['catalog_item_edit_pict_add_mark_for_source'] = 'Watermark to the INPUT image';

$il['catalog_clear_label'] = 'Clear';

$il['catalog_group_name_label'] = 'Name';
$il['catalog_group_id_label'] = 'Identifier';
$il['catalog_group_props_label'] = 'Properties';
$il['catalog_group_namedb_label'] = 'Name (DB)';

$il['catalog_link_details_label'] = 'More ...';

// labels, messages, hints in the admin panel
$il['catalog_category_name_label'] = 'Category Name';
$il['catalog_categories_build_from_cat_label'] = 'Build start category';
$il['catalog_categories_build_from_depth_label'] = 'Build start level';
$il['catalog_categories_open_levels_show_label'] = 'Number of expandable menu levels';
$il['catalog_categories_max_levels_show_label'] = 'Max. number of displayed menu levels';
$il['catalog_category_add_label'] = 'Add category';
$il['catalog_category_new_name'] = 'New category';
$il['catalog_category_del_alert'] = 'Do you really want to delete the category';
$il['catalog_category_remove_label'] = 'Delete category';
$il['catalog_edit_item_label'] = 'Editing a Product';
$il['catalog_item_order_label'] = 'Order';
$il['catalog_item_name_label'] = 'Name';
$il['catalog_item_txt_label'] = 'Description';
$il['catalog_item_added_msg'] = 'Product added';
$il['catalog_item_saved_msg'] = 'The item was saved';
$il['catalog_item_del_alert'] = 'Do you really want to delete the item?';
// $il['catalog_item_deleted_msg'] = 'Item Deleted';

$il['catalog_group_items_label'] = 'Products';
$il['catalog_add_prop_label'] = 'Add property';
$il['catalog_save_order_label'] = 'Save Order';
$il['catalog_OK_label'] = 'OK';
$il['catalog_button_show'] = 'Display';

$il['catalog_menu_label_import_commerceml'] = 'Import from CommerceML';
$il['catalog_commerceml_type'] = 'Type of import';
$il['catalog_commerceml_type_import'] = 'Product information (import.xml)';
$il['catalog_commerceml_type_offers'] = 'Only prices (offers.xml)';
$il['catalog_commerceml_do_import'] = 'Import';
$il['catalog_commerceml_xmlfile'] = 'File';
$il['catalog_commerceml_itemprops_assoc'] = 'Field Matching';
$il['catalog_commerceml_name_1C'] = 'Field in 1C';
$il['catalog_commerceml_name_santa'] = 'Field in santafox';
$il['catalog_commerceml_add_assocline'] = 'Add';
$il['catalog_commerceml_price_field'] = 'Price';
$il['catalog_commerceml_price_per_field'] = 'Price for';
$il['catalog_commerceml_price_per_field_no_matters'] = '- do not use--';
$il['catalog_commerceml_pricetype'] = 'Name of the price type in 1C';
$il['catalog_commerceml_category'] = 'Category for new';
$il['catalog_commerceml_group'] = 'ToGroup';
$il['catalog_commerceml_1C_ID_santafield'] = 'Directory property that stores the ID from 1C';
$il['catalog_commerceml_name_santafield'] = 'Directory property where the name is stored';
$il['catalog_commerceml_error_nofile'] = '<b style = "color: red;"> Error: File not loaded </ b>';
$il['catalog_commerceml_error_malformed_xml'] = '<b style = "color: red;"> Error: Incorrect XML </ b>';
$il['catalog_commerceml_error_reqfields_not_filled'] = '<b style = "color: red;"> Error: Required fields are not required </ b>';
$il['catalog_commerceml_error_no_group'] = '<b style = "color: red;"> Error: Comrade is not found. group </ b> ';
$il['catalog_commerceml_error_no_pricetype_found'] = '<b style = "color: red;"> Error: Specified price type not found </ b>';
$il['catalog_commerceml_import_completed'] = '<b> The import is complete. Items added:% added%, updated:% updated% </ b> ';
$il['catalog_commerceml_offers_completed'] = '<b> Prices updated (% updated%) </ b>';

$il['catalog_no_group_template_list'] = 'Product group does not have a product list output template';
$il['catalog_show_items_list_no_items'] = 'No products';
$il['catalog_delete_group_alert'] = 'Really delete the com. group ';

$il['catalog_prop_saved_msg'] = 'Property is saved';
$il['catalog_quicksearch_label'] = 'Quick Search';
$il['catalog_pub_compare'] = 'Compare Products';
$il['catalog_compare_max_items'] =' Max. number of goods being compared ';
$il['catalog_groups_list'] = 'List of product groups';
$il['catalog_groups_list_delete_only_empty'] = 'You can only delete a product group without products';
$il['catalog_list_group_items_count_label'] = 'Items';
$il['catalog_edit_category_hide_from_waysite_label'] = 'Hide On the Road';
$il['catalog_edit_category_hide_from_site_label'] = 'Do not show on website';

$il['catalog_set_type_64_max_error_msg'] = 'The SET data type does not allow more than 64 possible values';
$il['catalog_not_uniq_main_prop_save'] = 'The field% fieldname% is marked as unique, but a non-unique value is set';
$il['catalog_onsite_th'] = 'On the site?';
$il['catalog_list_set_available'] = 'Show on site';
$il['catalog_list_set_unavailable'] = 'Do not show on site';
$il['catalog_move2group_header'] = 'Move to Group';
$il['catalog_move2group_do'] = 'Move';
$il['catalog_item_moved_to_group'] = 'Moved';

$il['catalog_property_ending_basket'] = 'Endings for elements';
$il['catalog_property_ending_basket_descr'] = 'Correct endings in the item`s basket (separated by commas for digits 1,2,5)';

$il['catalog_categories_build_from_cat_first_label'] = 'Current category root';
$il['catalog_categories_build_need_add_way_label'] = 'Use categories on the road';

$il['catalog_order_email_confirm_tpl'] = 'Letter-status "Filled out, not confirmed"';
$il['catalog_order_email_confirmed_tpl'] = 'Status letter "Confirmed, not paid"';
$il['catalog_order_email_payment_tpl'] = 'Status letter "Paid in processing"';
$il['catalog_order_email_shipping_tpl'] = 'Letter-Status "In Delivery"';
$il['catalog_order_email_delivered_tpl'] = 'Letter-status "Delivered"';
$il['catalog_order_email_canceled_tpl'] = 'Status letter "Canceled"';

$il['catalog_order_not_found'] = 'Order not found!';
$il['catalog_order_updated'] = 'Update date';
$il['catalog_order_status'] = 'Order status';
$il['catalog_order_status_notify'] = 'Notify Customer';
$il['catalog_order_status_not_furnished'] = 'Not decorated';
$il['catalog_order_status_confirm'] = 'Not checked, not confirmed';
$il['catalog_order_status_confirmed'] = 'Confirmed, not paid';
$il['catalog_order_status_payment'] = 'Paid in processing';
$il['catalog_order_status_shipping'] = 'In delivery';
$il['catalog_order_status_delivered'] = 'Delivered';
$il['catalog_order_status_canceled'] = 'Canceled';