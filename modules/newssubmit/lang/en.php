<?php
$type_langauge = 'en';

$il['newssubmit_name_modul_base_name'] = 'Newsletter';
$il['newssubmit_name_modul_base_name1'] = 'Newsletter';

$il['module_newssubmit_menu_label'] = 'Manage Mailing List';
$il['module_newssubmit_menu_show_users'] = 'Subscriber List';
$il['module_newssubmit_menu_add_user'] = 'Add Subscriber';
$il['module_newssubmit_menu_show_news_submit'] = 'Send newsletter';


$il['module_newssubmit_label_propertes1'] = 'Subscription page for news';
$il['module_newssubmit_label_propertes2'] = 'The electronic address of outgoing messages';
$il['module_newssubmit_label_propertes7'] = 'Name from which the messages are sent';
$il['module_newssubmit_label_propertes3'] = 'Email caption when requesting a subscription';
$il['module_newssubmit_label_propertes4'] = 'Email header when sending directly';
$il['module_newssubmit_label_propertes5'] = 'If there is a mark, then when sending news, do not put a mark that they were sent (used in testing)';
$il['module_newssubmit_label_propertes6'] = 'Distribution Template';
$il['module_newssubmit_label_propertes_prefix'] = 'Prefix to generate unique distribution control codes';


$il['newssubmit_pub_formsubmit_show'] = 'Subscription Form';
$il['module_newssubmit_metod1_param1'] = 'Subscription form template';
$il['module_newssubmit_metod1_param2'] = 'Template of the subscription management form';
$il['module_newssubmit_metod1_param3'] = 'Automatically display the subscription management form for authorized users';

$il['module_newssubmit_admin_table_label_num'] = 'Number';
$il['module_newssubmit_admin_table_label_check'] = 'Mark';
$il['module_newssubmit_admin_table_label_name'] = 'Subscriber name';
$il['module_newssubmit_admin_table_label_mail'] = 'Subscriber`s address';
$il['module_newssubmit_admin_table_label_news'] = 'News feeds';
$il['module_newssubmit_admin_but_delete_users'] = 'Delete checked out';

$il['module_newssubmit_admin_table_submit_label_num'] = 'Number';
$il['module_newssubmit_admin_table_submit_label_check'] = 'Mark';
$il['module_newssubmit_admin_table_submit_label_date_news'] = 'Date of the news';
$il['module_newssubmit_admin_table_submit_label_label'] = 'Header';
$il['module_newssubmit_admin_table_submit_label_descrip'] = 'Short Description';
$il['module_newssubmit_admin_table_submit_text_start'] = 'The text shown at the beginning of the distribution:';
$il['module_newssubmit_admin_table_submit_text_end'] = 'The text shown at the end of the list:';
$il['module_newssubmit_admin_table_submit_bt_send'] = 'Distribute';

$il['newssubmit_admin_user_control_label1'] = 'Manage user`s subscription';
$il['newssubmit_admin_user_control_date_sub'] = 'Subscription Date:';
$il['newssubmit_admin_user_control_name_user'] = 'Subscriber Name:';
$il['newssubmit_admin_user_control_email_user'] = 'Email address for mailing:';
$il['newssubmit_admin_user_control_user_onoff'] = 'Pause the newsletter:';
$il['newssubmit_admin_user_control_news_label'] = 'Signed on:';
$il['newssubmit_admin_user_control_action_save'] = 'Save';
$il['newssubmit_admin_user_control_action_activate'] = 'Activate';
$il['newssubmit_admin_user_control_action_delete'] = 'Delete';
$il['newssubmit_admin_user_control_not_activate'] = '(not yet activated)';
$il['newssubmit_admin_user_control_activate'] = '(not yet activated)';

$il['module_newssubmit_max_letters_per_run'] = 'Max. number of letters sent at once (cron2) ';
$il['module_newssubmit_add2cron2_label'] = 'Add to cron2';

$il['newssubmit_uname_error'] = 'Error in the user name (4 to 255 characters)';
$il['newssubmit_email_error'] = 'Incorrect email address';
$il['newssubmit_no_news_error'] = 'You must mark at least one newsgroup';
$il['newssubmit_user_saved'] = 'Subscriber saved';
$il['newssubmit_user_activated'] = 'Subscriber activated';
$il['newssubmit_user_deleted'] = 'Subscriber removed';
$il['newssubmit_user_found'] = 'Subscriber found';
$il['newssubmit_user_not_found'] = 'Subscriber not found';
$il['newssubmit_find_user_by_email'] = 'find a subscriber by email';
$il['newssubmit_do_search_user'] = 'search';