<?php
/**
 * Основной управляющий класс модуля «Вопросы и Ответы»
 *
 * Модуль предназначен для орагиназции FAQ и возможности пользователей
 * задавать новые вопросы
 * @copyright ArtProm (с) 2001-2011
 * @author Александр Ильин [Comma] mecomayou@mail.ru , s@nchez s@nchez.me
 * @contributors Oslix <oslix@yandex.ru>, Alexsander Vyshnyvetskyy (alex_wdmg) <wdmg.com.ua@gmail.com>, Rinat <mail.rinat@yandex.ru>, bubek <bvvtut@tut.by>, digi_neiker, sanchez <sanchezby@gmail.com>
 * @version 2.0
 */

require_once realpath(dirname(__FILE__)."/../../")."/include/basemodule.class.php";

class faq extends BaseModule
{
    var $template_array = array();                      // Содержит распаршенный шаблон
    var $one_admin = false;                             // Одна админка
    var $path_templates = "modules/faq/templates_user"; // Путь, к шаблонам модуля
	private $offset_param_name = "offset";


    /**
     * Публичный метод для отображения списка разделов
     *
     */
    function pub_show_partitions($items_pagename='', $template='', $ifcounter = false)
    {
        global $kernel;
        if (empty($template))
        {
            $template = $kernel->pub_modul_properties_get('template');
            if (!$template['isset'])
               return '[#faq_modul_errore1#]';
            $template = $template['value'];
        }
        $this->set_templates($kernel->pub_template_parse($template));

        $page = $items_pagename;
        if (empty($page))
            $page = $kernel->pub_page_current_get();

        // Данные из MySQL о категриях
        $partitions = $this->get_partitions();

        // Получаем текущий раздел, если есть
    	$get_values = $kernel->pub_httpget_get();
    	if (!isset($get_values['b']))
    	    $pid = false;
        else
            $pid = intval($get_values['b']);
		
		if (!$pid)
			$def_link = $this->get_template_block('partition_list_all_active');
		else
			$def_link = $this->get_template_block('partition_list_all');
		
		if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
			$def_link = str_replace('%link%', "/".$page."/", $def_link);
		else
			$def_link = str_replace('%link%', $page.".html", $def_link);
		
		if($ifcounter) {
			$count = $this->get_items_count(false);

			if($count > 0)
				$counter = $this->get_template_block('partition_list_counter');
			else
				$counter = $this->get_template_block('partition_list_counter_null');

			$counter = str_replace('%count%', $count, $counter);
			$def_link = str_replace('%counter%', $counter, $def_link);
		} else {
			$def_link = str_replace('%counter%', '', $def_link);
		}
		
        // Формирование контента
        $lines = '';
        foreach ($partitions as $data)
        {
			// Формирование ЧПУ-ссылки
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
				$link = '/'.$page.'/'.$kernel->pub_pretty_url($data['name']).'-a'.$data['id'].'/';
			else
				$link = $page.'.html?a=2&b='.$data['id'];
			
            $line = $this->get_template_block('partition_list_line');
			
			if($pid == $data['id'])
				$line = $this->get_template_block('partition_list_line_active');
			else
				$line = $this->get_template_block('partition_list_line');
			
            $line = str_replace("%name%", $data['name'],$line);
            $line = str_replace("%link%", $link, $line);
			
			if($ifcounter) {
				$count = $this->get_items_count($data['id']);

				if($count > 0)
					$counter = $this->get_template_block('partition_list_counter');
				else
					$counter = $this->get_template_block('partition_list_counter_null');

				$counter = str_replace('%count%', $count, $counter);
				$line = str_replace('%counter%', $counter, $line);
			} else {
				$line = str_replace('%counter%', '', $line);
			}
			
            $lines .= $line;
        }

        $html  = $this->get_template_block('partition_list_begin');
        $html .= $def_link.$lines;
        $html .= $this->get_template_block('partition_list_end');

        return $html;
    }


    /**
     * Метод управления выводом FAQ
     *
     * @param boolean $form false - не выводить форму вопроса для посетителя; true - выводить
     * @param string $template
     * @param integer $limit
     * @param string $page
     * @return string
     */
    function pub_faq($form = false, $template='', $limit=0, $sitepage='', $pages=5, $sortorder='')
    {
    	global $kernel;
		$moduleid = $kernel->pub_module_id_get();
    	$get_values = $kernel->pub_httpget_get();
		
        if (empty($sitepage))
            $sitepage = $kernel->pub_page_current_get();
		
        if (empty($template))
        {
            $template = $kernel->pub_modul_properties_get('template');
            if (!$template['isset'])
                return '[#faq_modul_errore1#]';
            
			$template = $template['value'];

        }
        $this->set_templates($kernel->pub_template_parse($template));

		// Параметры постраничной навигации
		$perpage = intval($limit);
		if ($perpage < 1)
			$perpage = 10;

		$offset = intval($kernel->pub_httpget_get($this->offset_param_name));
		$page_num = intval($kernel->pub_httpget_get('page'));
    	
		// Получаем текущий раздел, если есть
    	$get_values = $kernel->pub_httpget_get();
    	if (!isset($get_values['b']))
    	    $pid = false;
        else
            $pid = intval($get_values['b']);
		
		if ($pid)
			$cond = "`pid`='".$pid."'";
		else
            $cond = true;
		
		// Получаем количество записей
		$crec = $kernel->db_get_record_simple("_".$moduleid."_content", $cond, "count(*) AS count");
		if ($crec)
			$total = intval($crec['count']);
		else
            $total = 0;
		
		if((empty($offset) && !empty($page_num)) && (defined("USE_PRETTY_URL") && USE_PRETTY_URL))
			$offset = $perpage * ($page_num-1);

    	if (!isset($get_values['a']))
    	    $type = 2;
        else
            $type = intval($get_values['a']);

    	if (!$pid)
    	    $pid = 0;
		else
    	    $pid = $pid;

    	switch ($type)
    	{
    	    //Отобразим список вопросов в разделе
            default:
    		case 2:
				
				// Получим листинг с вопросами/ответами
                $html = $this->create_questions($pid, $sitepage, $offset, $limit, $sortorder);
				
				// Сформируем ссылки пагинации
				if($pid > 0) {

					$partition = $this->get_partition($pid);
					if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
					  $purl = '/'.$sitepage.'/'.$kernel->pub_pretty_url($partition['name']).'-a'.$partition['id'].'/page-';
					else
					  $purl = $sitepage.'.html?a=2&b='.$pid.'&'.$this->offset_param_name.'=';

				} else {

					if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
					  $purl = "/".$sitepage.'/page-';
					else
					  $purl = $sitepage.'.html?'.$this->offset_param_name.'=';

				}
				
				// и выведем их
				$html = str_replace('%pages%', $this->build_pages_nav($total, $offset, $perpage, $purl, $pages, 'link'), $html);
                break;

            //отобразим конкретный вопрос и ответ на него
    		case 3:
                $html = $this->create_form_question($get_values, $page);
				$html = str_replace('%pages%', '', $html);
    			break;

    	}
		// Очистим для общей страницы вывода вопросов из всех разделов
		$html = str_replace("%partition_name%", '', $html);
		$html = str_replace("%partition_description%", '', $html);

        if ($form)
            $html .= $this->pub_form($template);

    	return $html;
    }

    /**
     * Возвращает форму для задания вопроса
     *
     * @param string $template
     * @return string
     */
    function pub_form($template='')
    {
        global $kernel;

        if (empty($template))
        {
            $template = $kernel->pub_modul_properties_get('template');
            if (!$template['isset'])
                return '[#faq_modul_errore1#]';
            $template = $template['value'];
        }

        $this->set_templates($kernel->pub_template_parse($template));

        $moduleid = $kernel->pub_module_id_get();
        $postvars = $kernel->pub_httppost_get();
        $user = htmlspecialchars($kernel->pub_httppost_get('faq_user_name',false));
        $mail = htmlspecialchars($kernel->pub_httppost_get('faq_user_email',false));
        $quest = nl2br(htmlspecialchars($kernel->pub_httppost_get('faq_user_question',false)));

        if (isset($postvars['faq_user_button']) && !empty($user) && !empty($quest))
        {
            $sql = 'INSERT INTO `'.$kernel->pub_prefix_get().'_'.$moduleid.'_content` (
            `id`,`pid`, `description`, `answer`, `user`, `email`,`added`)
            VALUES (
                NULL,0,
            \''.$kernel->pub_str_prepare_set($quest).'\',
            NULL,
            \''.$kernel->pub_str_prepare_set($user).'\',
            \''.$kernel->pub_str_prepare_set($mail).'\',
            "'.date("Y-m-d H:i:s").'");';
            $kernel->runSQL($sql);
            $adminEmail = $kernel->pub_modul_properties_get('email');
            if ($adminEmail['isset'])
            {//не пустой email админа
                $emails = explode(",",$adminEmail['value']);
                $mail_body = $this->get_template_block('email2admin');
                $mail_body = str_replace("%question%",$quest, $mail_body);
                $mail_body = str_replace("%user%",$user, $mail_body);
                $mail_body = str_replace("%email%",$mail, $mail_body);
                $subj = $kernel->pub_modul_properties_get('new_question_email_subj');
                if ($subj['isset'])
                    $subj = $subj['value'];
                else
                    $subj = 'Новый вопрос';
                foreach ($emails as $email)
                {
                    $email = trim($email);
                    $kernel->pub_mail(array($email), array($email), $email, 'admin', $subj, $mail_body, false);
                }
            }

            $html = $this->get_template_block('form_submit_ok');
        }
        else
            $html = $this->get_template_block('form');

    	return $html;
    }

    /**
     * Публичное действие для отображения `title`
     *
     * @param string $title_def Заголовок по умолчанию,
	 * например для страниц с листингом вопросов и ответов
     * @return string
     */
    public function pub_show_title($title_def)
    {
        global $kernel;
		
    	$get_values = $kernel->pub_httpget_get();
    	if (!isset($get_values['b']))
    	    $pid = false;
        else
            $pid = intval($get_values['b']);
			
    	if (!isset($get_values['a']))
    	    $type = 2;
        else
            $type = intval($get_values['a']);
			
		if($type == 2) {
			if($pid > 0) {
				$partition = $this->get_partition($pid);
				if (!$partition)
					return trim($title_def);
				
				return $partition['name'];
			} else {
				return trim($title_def);
			}
		} else {
			$data = $this->get_question(intval($get_values['b']));
			if (!$data)
				return trim($title_def);
			
			return $data['name'];
		}
		
    }

    /**
     * Публичное действие для отображения `meta-keywords`
     *
     * @param string $meta_keywords_def Мета-keywords по умолчанию,
	 * например для страниц с листингом вопросов и ответов
     * @return string
     */
    public function pub_show_meta_keywords($meta_keywords_def)
    {
        global $kernel;
		
    	$get_values = $kernel->pub_httpget_get();
    	if (!isset($get_values['b']))
    	    $pid = false;
        else
            $pid = intval($get_values['b']);
			
    	if (!isset($get_values['a']))
    	    $type = 2;
        else
            $type = intval($get_values['a']);
			
		if($type == 2) {
			if($pid > 0) {
				$partition = $this->get_partition($pid);
				if (!$partition)
					return trim($meta_keywords_def);
				
				return $partition['meta-keywords'];
			} else {
				return trim($meta_keywords_def);
			}
		} else {
			$data = $this->get_question(intval($get_values['b']));
			if (!$data)
				return trim($meta_keywords_def);
			
			return $data['meta-keywords'];
		}
		
    }

    /**
     * Публичное действие для отображения `meta-description`
     *
     * @param string $meta_description_def Мета-описание по умолчанию,
	 * например для страниц с листингом вопросов и ответов
     * @return string
     */
    public function pub_show_meta_description($meta_description_def)
    {
        global $kernel;
		
    	$get_values = $kernel->pub_httpget_get();
    	if (!isset($get_values['b']))
    	    $pid = false;
        else
            $pid = intval($get_values['b']);
			
    	if (!isset($get_values['a']))
    	    $type = 2;
        else
            $type = intval($get_values['a']);
			
		if($type == 2) {
			if($pid > 0) {
				$partition = $this->get_partition($pid);
				if (!$partition)
					return trim($meta_description_def);
				
				return $partition['meta-description'];
			} else {
				return trim($meta_description_def);
			}
		} else {
			$data = $this->get_question(intval($get_values['b']));
			if (!$data)
				return trim($meta_description_def);
			
			return $data['meta-description'];
		}
		
    }

    function get_partitions()
    {
        global $kernel;
		$moduleid = $kernel->pub_module_id_get();
        return $kernel->db_get_list_simple("_".$moduleid."_partitions", "true");
    }

    function get_items_count($pid=false)
	{
		global $kernel;
		$moduleid = $kernel->pub_module_id_get();
		$total = 0;
		
		if($pid)
			$crec = $kernel->db_get_record_simple("_".$moduleid."_content", "`pid`='".$pid."'", "count(*) AS count");
		else
			$crec = $kernel->db_get_record_simple("_".$moduleid."_content", "true", "count(*) AS count");
		
		if ($crec)
			$total = $crec['count'];
		
		return $total;
	}
	
    function get_partition_questions($pid, $offset, $limit=0, $only_actual=false, $sortorder=false)
    {
        global $kernel;
		$moduleid = $kernel->pub_module_id_get();
		
		if ($sortorder == 'new_at_top')
			$sorting = " ORDER BY `added` DESC";
		elseif ($sortorder == 'new_at_bottom')
			$sorting = " ORDER BY `added` ASC";
		elseif ($sortorder == 'desc')
			$sorting = " ORDER BY `id` DESC";
		else
			$sorting = " ORDER BY `id` ASC";
			
        if ($pid==0)
        {
            if ($only_actual)
                $cond = "`answer` IS NOT NULL";
            else
                $cond = "true";
			
            $cond .= $sorting;
        }
        else
        {
            $cond = "`pid`='".$pid."'".$sorting;
            if ($only_actual)
                $cond = "`answer` IS NOT NULL AND ".$cond;
        }
		
        return $kernel->db_get_list_simple("_".$moduleid."_content", $cond, "*", $offset, $limit);
    }

    /**
     * Формирует HTML с вопросами выбранного раздела
     *
     * @param integer $pid
     * @param string $page
     * @param integer $limit
     * @return string
     */
    function create_questions($pid, $page, $offset, $limit, $sortorder)
    {
        global $kernel;
        $questions = $this->get_partition_questions($pid, $offset, $limit, true, $sortorder);
        // Формирование контента
        $lines = '';
        $i=1;
        foreach ($questions as $data)
        {
			
            $line = $this->get_template_block('partition_content_line');
			
			if(!empty($data['email'])) {
				$email = $this->get_template_block('partition_content_email');
				if($email) {
					$email = str_replace("%email%", $data['email'], $email);
					$line = str_replace("%email%", $email, $line);
				} else {
					$line = str_replace("%email%", $data['email'], $line);
				}
			} else {
				$email = $this->get_template_block('partition_content_email_null');
				if($email) {
					$line = str_replace("%email%", $email, $line);
				} else {
					$line = str_replace("%email%", "", $line);
				}
			}
			
			if(!empty($data['added'])) {
				$added = $this->get_template_block('partition_content_added');
				if($added) {
					$added = str_replace("%added%", $data['added'], $added);
					$line = str_replace("%added%", $added, $line);
				} else {
					$line = str_replace("%added%", $data['added'], $line);
				}
			} else {
				$added = $this->get_template_block('partition_content_added_null');
				if($added) {
					$line = str_replace("%added%", $added, $line);
				} else {
					$line = str_replace("%added%", "", $line);
				}
			}
			
			if(!empty($data['question'])) {
				$question = $this->get_template_block('partition_content_question');
				if($question) {
					$question = str_replace("%question%", $data['question'], $question);
					$line = str_replace("%question%", $question, $line);
				} else {
					$line = str_replace("%question%", $data['question'], $line);
				}
			} else {
				$question = $this->get_template_block('partition_content_question_null');
				if($question) {
					$line = str_replace("%question%", $question, $line);
				} else {
					$line = str_replace("%question%", "", $line);
				}
			}
				
			if(!empty($data['description'])) {
				$description = $this->get_template_block('partition_content_description');
				if($description) {
					$description = str_replace("%description%", $data['description'], $description);
					$line = str_replace("%description%", $description, $line);
				} else {
					$line = str_replace("%description%", $data['description'], $line);
				}
			} else {
				$description = $this->get_template_block('partition_content_description_null');
				if($description) {
					$line = str_replace("%description%", $description, $line);
				} else {
					$line = str_replace("%description%", "", $line);
				}
			}
			
			if(!empty($data['answer'])) {
				$answer = $this->get_template_block('partition_content_answer');
				if($answer) {
					$answer = str_replace("%answer%", $data['answer'], $answer);
					$line = str_replace("%answer%", $answer, $line);
				} else {
					$line = str_replace("%answer%", $data['answer'], $line);
				}
			} else {
				$answer = $this->get_template_block('partition_content_answer_null');
				if($answer) {
					$line = str_replace("%answer%", $answer, $line);
				} else {
					$line = str_replace("%answer%", "", $line);
				}
			}
			
			if(!empty($data['user'])) {
				$user = $this->get_template_block('partition_content_user');
				if($user) {
					$user = str_replace("%user%", $data['user'], $user);
					$line = str_replace("%user%", $user, $line);
				} else {
					$line = str_replace("%user%", $data['user'], $line);
				}
			} else {
				$user = $this->get_template_block('partition_content_user_null');
				if($user) {
					$line = str_replace("%user%", $user, $line);
				} else {
					$line = str_replace("%user%", "", $line);
				}
			}

			// Формирование ЧПУ-ссылки
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
				$link = '/'.$page.'/'.$kernel->pub_pretty_url($data['question']).'-q'.$data['id'];
			else
				$link = $page.'.html?a=3&b='.$data['id'];
			
            $line = str_replace("%link%", $link, $line);
			$line = str_replace("%num%", $i++, $line);
            $lines .= $line;
        }

        $html = $this->get_template_block('partition_content_begin');
        $html .= $lines;
        $html .= $this->get_template_block('partition_content_end');

        $partition = $this->get_partition($pid);
        if ($partition)
        {

			// Выведим название раздела
			$partition_name = $this->get_template_block('partition_name');
			$partition_name = str_replace("%name%", $partition['name'], $partition_name);
			if($partition['description'])
				$html = str_replace("%partition_name%", $partition_name, $html);
			else
				$html = str_replace("%partition_name%", '', $html);
			
			// Выведим описание раздела
			$partition_description = $this->get_template_block('partition_description');
			$partition_description = str_replace("%description%", $partition['description'], $partition_description);
			if($partition['description'])
				$html = str_replace("%partition_description%", $partition_description, $html);
			else
				$html = str_replace("%partition_description%", '', $html);
			
            // Добавим раздел в дорогу сайта и сформируем ЧПУ-ссылку
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
				$kernel->pub_waysite_set(array('url' => '/'.$page."/", 'caption' => $partition['name']));
			else
				$kernel->pub_waysite_set(array('url' => $page.".html", 'caption' => $partition['name']));
			
            //Добавим в тайтл раздел
            $kernel->pub_page_title_add($partition['name']);
        }
        return $html;
    }

    function get_question($id)
    {
        global $kernel;
        return $kernel->db_get_record_simple("_".$kernel->pub_module_id_get()."_content","id=".$id);
    }

    function get_partition($id)
    {
        global $kernel;
        return $kernel->db_get_record_simple("_".$kernel->pub_module_id_get()."_partitions","id=".$id);
    }

    /**
     * Формирует форму с конкретным вопросом и ответом на него
     *
     * @param array $get_values массив переменных _GET
     * @param string $page
     * @return string
     */
    function create_form_question($get_values, $page='')
    {
        global $kernel;
        $data = $this->get_question(intval($get_values['b']));
        if (!$data)
            $kernel->pub_redirect_refresh_global("/".$page.".html");

        // Формирование контента
        $line = $this->get_template_block('answer_table');
        $line = str_replace("%question%", $data['question'],$line);
        $line = str_replace("%description%", $data['description'],$line);
        $line = str_replace("%answer%", $data['answer'],$line);
        $line = str_replace("%user%", $data['user'],$line);
        $line = str_replace("%email%", $data['email'],$line);
        $line = str_replace("%added%", $data['added'],$line);
        $line = str_replace("%page%", $page,$line);
        $line = str_replace("%id%", $data['pid'],$line);


        $partition = $this->get_partition(intval($data['pid']));

		// Добавим раздел в дорогу сайта и сформируем ЧПУ-ссылку
		if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
			$kernel->pub_waysite_set(array('url' => '/'.$page."/".$kernel->pub_pretty_url($partition['name']).'-a'.$data['pid'].'/', 'caption' => $partition['name']));
			$kernel->pub_waysite_set(array('url' => '/'.$page."/".$kernel->pub_pretty_url($data['question']).'-q'.$data['id'].'/', 'caption' => $data['question']));
		} else {
			$kernel->pub_waysite_set(array('url' => $page.'.html?a=2&b='.$data['pid'], 'caption' => $partition['name']));
			$kernel->pub_waysite_set(array('url' => $page.'.html?a=3&b='.$data['id'], 'caption' => $data['question']));
		}

        //Добавим информацию в тайтл
        $kernel->pub_page_title_add($partition['name']);
        $kernel->pub_page_title_add($data['question']);

        return $line;
    }

    function priv_show_date_picker()
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse('modules/faq/templates_admin/date_picker.html'));
        $content = $this->get_template_block('date_picker');
        return $content;
    }

    /**
     * Функция для построения меню для административного интерфейса
     *
     * @param pub_interface $menu Обьект класса для управления построением меню
     * @return boolean
     */
	public function interface_get_menu($menu)
	{
        $menu->set_menu_block('[#news_menu_label#]');
        $menu->set_menu("[#faq_menu_list#]","show_list");
        $menu->set_menu("[#faq_menu_users_questions#]","users_questions");
        $menu->set_menu("[#faq_menu_partitions#]","partitions");
        $menu->set_menu_default('users_questions');
        $menu->set_menu_block('[#faq_date_block#]');
        $menu->set_menu_plain($this->priv_show_date_picker());
        $this->priv_show_date_picker();
	    return true;
	}


    /**
     * Предопределйнный метод, используется для вызова административного интерфейса модуля
     *
     * @return string
     */
    function start_admin()
    {
        global $kernel;

        $get_values = $kernel->pub_httpget_get();
        $moduleid = $kernel->pub_module_id_get();
        
        $template = "modules/faq/templates_admin/container.html";
        $this->set_templates($kernel->pub_template_parse($template));
        
        $container_begin = $this->get_template_block('container_begin');
        $container_end = $this->get_template_block('container_end');
        
        $html = '';
        $view = $kernel->pub_section_leftmenu_get();
        switch ($view)
        {
            case "partitions":
                $template = "modules/faq/templates_admin/partitions.html";
                $this->set_templates($kernel->pub_template_parse($template));
                $partitions = $this->get_partitions();
                $lines = '';
                foreach ($partitions as $value)
                {
                    $line = $this->get_template_block('row');
                    $line = str_replace("%pid%", $value['id'], $line);
                    $line = str_replace("%name%", $value['name'], $line);
                    $line = str_replace("%action_edit%", "partition_edit&id=".$value['id'], $line);
                    $line = str_replace("%action_delet%", "partition_delete&id=".$value['id'], $line);
                    $line = str_replace("%action_addco%", "add_quest&partition_id=".$value['id'], $line);
                    $lines.=$line;
                }
                $html = $container_begin;
                $html .= $this->get_template_block('table');
                $html = str_replace('%rows%', $lines, $html);
                $html .=$this->priv_form_add_partition();
                $html .= $container_end;
                break;
            case "faq_import_csv":
                $partid = intval($_POST['csvpart']);
                if (is_uploaded_file($_FILES['csvfile']["tmp_name"]) && $partid>0)
                {

                    $handle = fopen($_FILES["csvfile"]["tmp_name"], "r");
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
                    {
                        if (count($data)!=5)
                            continue;
						
                        $sql = 'INSERT INTO `'.$kernel->pub_prefix_get().'_'.$moduleid.'_content` (
                            `id`, `pid`, `description`, `answer`, `question`, `user`, `email`, `added`, `meta-description`, `meta-keywords`)
                            VALUES (
                            NULL,
                            '.$partid.',
							"'.$kernel->pub_str_prepare_set($data[1]).'",
							"'.$kernel->pub_str_prepare_set($data[2]).'",
                            "'.$kernel->pub_str_prepare_set($data[0]).'",
							"",
							"",
							"'.date("Y-m-d H:i:s").'",
							"'.$kernel->pub_str_prepare_set($data[3]).'",
							"'.$kernel->pub_str_prepare_set($data[4]).'" )';
                        $kernel->runSQL($sql);
                    }
                    fclose($handle);
                }
                $kernel->pub_redirect_refresh_reload('show_list');
                break;
            //Добовляем новый раздел
            case "faq_add_partition":

                if (!isset($get_values['id']) || (empty($get_values['id'])))
                    $get_values['id'] = "NULL";

                $this->priv_partition_add($get_values['id']);
                $kernel->pub_redirect_refresh_reload('partitions');
                break;

            //Форма редактирования раздела
            case "partition_edit":
                $html = $container_begin;
                
                if (isset($get_values['id']))
                    $html .= $this->priv_form_add_partition($get_values['id']);
                    
                $html .= $container_end;
                break;

            //Удаляем раздел
            case "partition_delete":
                $pid = intval($get_values['id']);
                $sql = "DELETE FROM `".$kernel->pub_prefix_get()."_".$moduleid."_partitions`
                        WHERE `id`=".$pid."
                        LIMIT 1";
                $kernel->runSQL($sql);

                $sql = "DELETE FROM `".$kernel->pub_prefix_get()."_".$moduleid."_content`
                        WHERE `pid`=".$pid."";
                $kernel->runSQL($sql);
                $kernel->pub_redirect_refresh('partitions');
                break;

            //Выводим форму для редактирования или добавления вопроса
            case "add_quest":

                $sel_par_id = 0;
                if (isset($get_values['partition_id']))
        	       $sel_par_id = $get_values['partition_id'];

        	    $content_id = 0;
        	    if (isset($get_values['content_id']))
                    $content_id = $get_values['content_id'];
                $html = $container_begin;
                $html .= $this->priv_create_form_element($sel_par_id, $content_id);
                $html .= $container_end; 
                break;

            //Сохранение отредактированного вопроса
            case 'element_save':
                $content_id = intval($kernel->pub_httppost_get('id'));
                $this->priv_element_save($content_id);
                $kernel->pub_redirect_refresh_reload('show_list');
                break;

            //удаяляем ворпос из FAQ
            case "del_quest":

                if (isset($get_values['content_id']))
                {
                    $sql = "DELETE FROM `".$kernel->pub_prefix_get()."_".$moduleid."_content`
                            WHERE `id`=".$get_values['content_id'].";";
                    $kernel->runSQL($sql);
                }
                if ($kernel->pub_httpget_get('redirect_qusers'))
                    $kernel->pub_redirect_refresh('users_questions');
                else
                    $kernel->pub_redirect_refresh('show_list');
                break;

            //Отобразить список вопросов заданных с сайта
            default:
            case "users_questions":

                $template = "modules/faq/templates_admin/usersquestions.html";
                $this->set_templates($kernel->pub_template_parse($template));

                $sql = "SELECT content.*,partitions.name AS pname FROM ".$kernel->pub_prefix_get()."_".$moduleid."_content AS content
                        LEFT JOIN ".$kernel->pub_prefix_get()."_".$moduleid."_partitions AS partitions ON partitions.id=content.pid
                        WHERE content.answer IS NULL ORDER BY `added` DESC";
                $res = $kernel->runSQL($sql);

                if ($res)
                {
                    // Подготавливаем массив с разделами
                    $lines = '';
					if (mysqli_num_rows($res)>0) {
						while ($data = mysqli_fetch_assoc($res))
						{
							$line = $this->get_template_block('line');
							$line = str_replace("%user_name%",     $data['user'],        $line);
							$line = str_replace("%user_email%",    $data['email'],       $line);
							$line = str_replace("%question%",      $data['question'], $line);
							$line = str_replace("%description%",   $data['description'], $line);
							$line = str_replace("%content_id%",    $data['id'],          $line);
							$line = str_replace("%partition_id%",  $data['pid'],         $line);
							$line = str_replace("%partition_name%",$data['pname'],         $line);
							$line = str_replace("%added%",         $data['added'],         $line);
							$line = str_replace("%question_edit%", "add_quest&content_id=".$data['id']."&partition_id=".$data['pid'], $line);
							$line = str_replace("%action_delet_q%","del_quest&content_id=".$data['id']."&redirect_qusers=1", $line);

							$lines .= $line;
						}
					}
						
                    $html = $container_begin;
                    $html .= $this->get_template_block('begin').$lines.$this->get_template_block('end');
                    $html .= $container_end;
                    mysqli_free_result($res);
                }
                else
                    $html = $container_begin."[#faq_menu_users_questions_nonew#]".$container_end;

                break;

            // Выводим список разделов
            case "show_list":
                $html = $container_begin;
                $html .= $this->priv_create_list_partition();
                $html .= $container_end;
                break;
        }
		
		$html = str_replace('%submodul_name%', $this->get_current_module_name(), $html);
        return $html;
    }

    /**
     * Выводит форму для управления разделами
     *
     * @access private
     * @return string
     */
    function priv_create_list_partition()
    {
        global $kernel;

        // Предустановки
        $template = $kernel->pub_template_parse('modules/faq/templates_admin/form_admin_table.html');
        $list_table = $template["table"];

        $pid = intval($kernel->pub_httpget_get('pid'));
        $date = $kernel->pub_httpget_get('date');

        $moduleid = $kernel->pub_module_id_get();
        $sql = "SELECT content.*,partitions.name AS pname FROM ".$kernel->pub_prefix_get()."_".$moduleid."_content AS content
                LEFT JOIN ".$kernel->pub_prefix_get()."_".$moduleid."_partitions AS partitions ON partitions.id=content.pid ";

        if ($pid>0)
            $sql.=" WHERE pid=".$pid;
        elseif(!empty($date))
            $sql.=" WHERE `added`>='".$date." 00:00:00' AND `added`<='".$date." 23:59:59'";
        $sql.=" ORDER BY `added` DESC";

        $res = $kernel->runSQL($sql);
		if(!$res)
			return "[#faq_menu_users_questions_nonew#]";

        // Парсим шаблоны
        $lines='';
        $i=1;
        while ($value=mysqli_fetch_assoc($res))
        {
            $line = $template['row'];
            if (empty($value['user']))
                $value['user'] = "Аноним";
            $line = str_replace("%num%", $i++,$line);
            $line = str_replace("%added%", $value['added'],$line);
            $line = str_replace("%partition_name%", $value['pname'],$line);
            $line = str_replace("%question%", $value['question'],$line);
            $line = str_replace("%user_name%", $value['user'],$line);
            $line = str_replace("%user_email%", $value['email'],$line);
            $line = str_replace("%action_delet_q%", "del_quest&content_id=".$value['id'], $line);
            $line = str_replace("%question_edit%", "add_quest&content_id=".$value['id'], $line);
            if (empty($value['answer']))
                $line  = str_replace('%has_answer%', $template['no_answer'], $line);
            else
                $line  = str_replace('%has_answer%', $template['has_answer'], $line);


            $lines .= $line;
        }
        $list_table = str_replace('%rows%', $lines, $list_table);
        $list_table = str_replace('%total%', mysqli_num_rows($res), $list_table);
        mysqli_free_result($res);


        return $list_table;
    }

    /**
     * формирует форму для добавления или редактирования раздела
     *
     * @param integer|string $id ID
     * @return string
     */
    function priv_form_add_partition($id = "")
    {
        global $kernel;

        $template  = "modules/faq/templates_admin/form_add_them.html";

        $this->set_templates($kernel->pub_template_parse($template));

        $html = $this->get_template_block('table');

        //Возможно это редактирование и надо узнать текущее значение формы
        $value       = "";
        $form_label  = "[#faq_admin_table_label#]";
        $name_button = "[#faq_button_name_add_partition#]";
        if (!empty($id))
        {
            $part = $this->get_partition(intval($id));
            $form_label  = "[#faq_admin_table_label_edit#]";
            $name_button = "[#faq_button_save#]";
        } else {
			$part['name'] = '';
			$part['description'] = '';
			$part['meta-description'] = '';
			$part['meta-keywords'] = '';
		}

        $name = str_replace("%value%", $part['name'], $this->get_template_block('name'));
        $html = str_replace("%name%", $name, $html);

		$editor = new edit_content();
		$editor->set_edit_name('description');
		$editor->set_simple_theme(true);
		if (isset($part['description']))
			$editor->set_content($part['description']);
		else
			$editor->set_content('');
		
		
		$description = str_replace('%editor%', $editor->create(), $this->get_template_block('description'));
		$html = str_replace('%description%', $description, $html);
		
		$meta_description = str_replace("%value%", $part['meta-description'], $this->get_template_block('meta_description'));
        $html = str_replace("%meta-description%", $meta_description, $html);
		
		$meta_keywords = str_replace("%value%", $part['meta-keywords'], $this->get_template_block('meta_keywords'));
        $html = str_replace("%meta-keywords%", $meta_keywords, $html);
		
        $html = str_replace("%action%", $kernel->pub_redirect_for_form("faq_add_partition&amp;id=".$id), $html);
        $html = str_replace("%form_label%", $form_label, $html);
        $html = str_replace("%name_button%", $name_button, $html);
		
        //4 csv import
        if (empty($id))
        {
            $html .= $this->get_template_block('import');
            $parts = $this->get_partitions();
            $plines = "";
            foreach ($parts as $part)
            {
                $pline = $this->get_template_block('part_select_line');
                $pline = str_replace("%ovalue%", $part['id'], $pline);
                $pline = str_replace("%oname%", htmlspecialchars($part['name']), $pline);
                $plines.=$pline;
            }
            $html = str_replace("%action_import%", $kernel->pub_redirect_for_form("faq_import_csv"), $html);
            $html = str_replace("%part_select_lines%", $plines, $html);
        }
        return $html;
    }


    /**
     * Добавляет раздел или заменяет имя уже существующего
     *
     * @param integer $id ID раздела при изменении имени и NULL при добавлении нового
     * @return boolean
     */
    function priv_partition_add($id)
    {
        global $kernel;

        $pname = $kernel->pub_httppost_get('name',false);
        $pdescription = $kernel->pub_httppost_get('description', '');
        $pmetadescription = $kernel->pub_httppost_get('meta_description', '');
        $pmetakeywords = $kernel->pub_httppost_get('meta_keywords', '');
		
        if (empty($pname))
            return false;
		
        $sql = 'REPLACE `'.$kernel->pub_prefix_get().'_'.$kernel->pub_module_id_get().'_partitions`
                  (`id`, `name`, `description`, `meta-description`, `meta-keywords`)
                VALUES ('.$id.', \''.$kernel->pub_str_prepare_set(htmlspecialchars($pname)).'\', \''.$pdescription.'\', \''.$kernel->pub_str_prepare_set(htmlspecialchars($pmetadescription)).'\', \''.$kernel->pub_str_prepare_set(htmlspecialchars($pmetakeywords)).'\');';

        if (!$kernel->runSQL($sql))
            return false;

        return true;
    }


    function priv_create_form_element($partition = 0, $content_id = 0)
    {
        global $kernel;

        // Вызываем яваскрипт-форму для текста
        $content = new edit_content();
        $content->set_edit_name('faqcontent');
        $content->set_simple_theme(true);


        $template = "modules/faq/templates_admin/add_faq.html";
        $this->set_templates($kernel->pub_template_parse($template));

        $html = $this->get_template_block('table');

        if ($content_id > 0)//Значит это конкретный вопрос и узнаем что у него уже есть
            $result = $this->get_question(intval($content_id));
        else //новый вопрос
            $result = array('id'=>0,'question'=>'','user'=>'','email'=>'','description'=>'', 'meta_description'=>'', 'meta_keywords'=>'', 'answer'=>'','pid'=>0);

        $html = str_replace("%id%", $result['id'], $html);
        $html = str_replace("%question%", $result['question'], $html);
        $html = str_replace("%user_name%",$result['user'], $html);
        $html = str_replace("%user_email%",$result['email'], $html);
        $html = str_replace("%description%",$result['description'], $html);
        $html = str_replace("%meta_description%",$result['meta-description'], $html);
        $html = str_replace("%meta_keywords%",$result['meta-keywords'], $html);
        $content->set_content($result['answer']);

        // Подготовка массива для создания select'а
        if ($partition==0)
            $partition = $result['pid'];
        $partitions = $this->get_partitions();
        // Создаем select
        $catslist = '<select name="selected_category"><option value="0">--не выбрано--</option> ';
        foreach ($partitions as $data)
        {
            if ($data['id']==$partition)
                $catslist.='<option value="'.$data['id'].'" selected>'.$data['name'].'</option>';
            else
                $catslist.='<option value="'.$data['id'].'">'.$data['name'].'</option>';
        }
        $catslist.='</select>';
        $html = str_replace("%category_list%",$catslist,$html);


        $html = str_replace("%action%", $kernel->pub_redirect_for_form("element_save"), $html);
        $html = str_replace("%content_id%", $content_id, $html);
        $html = str_replace("%editor%", $content->create(), $html);

        return $html;
    }

    /**
     * Сохраняет отредактированный вопрос в базе
     *
     * @param integer $content_id ID вопроса, если он уже существует
     * @return boolean
     */
    function priv_element_save($content_id = 0)
    {
        global $kernel;

        $question = $kernel->pub_httppost_get('faq_add_content_question');
        $content = $kernel->pub_httppost_get('faqcontent');
        $user = $kernel->pub_httppost_get('user');
        $descr = $kernel->pub_httppost_get('faq_description');
        $meta_description = $kernel->pub_httppost_get('meta_description');
        $meta_keywords = $kernel->pub_httppost_get('meta_keywords');
        $email = $kernel->pub_httppost_get('email');
        $catid =  intval($kernel->pub_httppost_get('selected_category'));
        if ($content_id==0)
        {
            $sql = 'INSERT INTO `'.$kernel->pub_prefix_get().'_'.$kernel->pub_module_id_get().'_content`
                    (`pid`, `question`, `answer`, `user`, `email`, `description`,`meta-description`,`meta-keywords`,`added`)
                    VALUES
                    (
                    "'.$catid.'",
                    "'.$question.'",
                    "'.$content.'",
                    "'.$user.'",
                    "'.$email.'",
                    "'.$descr.'",
                    "'.$meta_description.'",
                    "'.$meta_keywords.'",
                    "'.date("Y-m-d H:i:s").'");';


        }
        else
        {
            $sql = 'UPDATE `'.$kernel->pub_prefix_get().'_'.$kernel->pub_module_id_get().'_content`
                    SET `pid`='.$catid.',
                        `question`="'.$question.'",
                        `answer`="'.$content.'",
                        `user`="'.$user.'",
                        `email`="'.$email.'",
                        `description`="'.$descr.'",
                        `meta-description`="'.$meta_description.'",
                        `meta-keywords`="'.$meta_keywords.'"
                    WHERE id='.$content_id;

            $questionRec = $this->get_question($content_id);

            //отправка письма если надо
            $adminEmail = $kernel->pub_modul_properties_get('email');

            if (empty($questionRec['answer']) && $kernel->pub_is_valid_email($email) && $adminEmail['isset'])
            {
                $adminEmail = explode(",",$adminEmail['value']);
                $adminEmail = $adminEmail[0];
                if (!$kernel->pub_is_valid_email($adminEmail))
                    $adminEmail = 'admin@'.$kernel->pub_http_host_get();

                $moduleTpl = "modules/faq/templates_admin/add_".$kernel->pub_module_id_get().".html";
                if (file_exists($moduleTpl))
                    $template = $moduleTpl;
                else
                    $template = "modules/faq/templates_admin/add_faq.html";
                
				$this->set_templates($kernel->pub_template_parse($template));
                $mail_body = $this->get_template_block('email2user');
                $mail_body = str_replace('%added%', $questionRec['added'], $mail_body);
                $mail_body = str_replace('%user%', $user, $mail_body);
                $mail_body = str_replace('%question%', $question, $mail_body);
                $mail_body = str_replace('%description%', $descr, $mail_body);
                $mail_body = str_replace('%answer%', $content, $mail_body);


                $subj = $kernel->pub_modul_properties_get('answer_email_subj');
                if ($subj['isset'])
                    $subj = $subj['value'];
                else
                    $subj = 'Ответ на ваш вопрос на сайте '.$kernel->pub_http_host_get();
                
				$kernel->pub_mail(array($email),
                                  array($email),
                                  $adminEmail,
                                  'admin',
                                  $subj,
                                  $mail_body,
                                  false
                                  );
            }
        }

        if (!$kernel->runSQL($sql))
            return false;
		
        return true;
    }
}
