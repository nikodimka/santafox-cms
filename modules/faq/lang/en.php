<?php
/**
 * Language file for the module "Questions and Answers"
 * @author Alexander Ilyin [Comma]
 * @copyright ArtProm © 2007
 * @version 1.0 beta
 */

$type_langauge	= 'en';

$il['faq_import_label1'] = 'Add questions and answers to the section';
$il['faq_import_label2'] = 'from the CSV file';
$il['faq_import_button_label'] = 'load';

$il['faq_modul_base_name'] = 'Questions and Answers';
$il['faq_modul_base_name1'] = 'Questions and Answers';
$il['faq_modul_metod_pub_faq'] = 'Show questions';

$il['faq_modul_errore1'] = 'Template file is not set in the module settings';


$il['faq_admin_table_label'] = 'Creating a new partition';
$il['faq_add_partition_name'] = 'Section name';
$il['faq_add_partition_description'] = 'Description of the section';
$il['faq_add_partition_meta_description'] = 'Meta description of the section';
$il['faq_add_partition_meta_keywords'] = 'Meta keywords';
$il['faq_button_name_add_partition'] = 'Add';
$il['faq_admin_table_label_edit'] = 'Editing Section';
$il['faq_button_save'] = 'Save';
$il['faq_list_edit_partition_name'] = 'Edit section';
$il['faq_partition_intems'] = 'elements';
$il['faq_list_delete_partition'] = 'Delete partition';
$il['faq_list_add_element'] = 'Add a new question';
$il['faq_list_del_element'] = 'Delete partition question';
$il['faq_list_delete_partition_alert'] = 'The partition and its contents will be deleted. Continue?';
$il['faq_list_delete_element_alert'] = 'The question will be deleted. Continue?';
$il['faq_list_edit_element'] = 'Edit Question';


$il['faq_modul_param1_name'] = 'Display form for entering a new question';
$il['faq_modul_param2_name'] = 'Send address of notifications about new user questions';
$il['faq_modul_param_list_pagename'] = 'Question Answer Answer Page';
$il['faq_modul_param_answer_mail_subj'] = 'The subject of the email to respond to users';


$il['faq_menu_users_questions_nonew'] = 'No questions from visitors';
$il['faq_menu_users_questions'] = 'Visitor Questions';
$il['faq_module_label_user_question'] = 'Website visitor`s questions';


$il['faq_menu_caption'] = 'Management';
$il['faq_menu_list'] = 'All questions';
$il['faq_list_header'] = 'Sections and elements';
$il['faq_list_manage'] = 'Management';

$il['faq_edit_categoty'] = 'Section';
$il['faq_edit_question'] = 'Question Header';
$il['faq_edit_content'] = 'Edit Item';
$il['faq_edit_description'] = 'Question text';
$il['faq_edit_editor'] = 'Answer the question';
$il['faq_edit_meta_description'] = 'Meta description of the answer';
$il['faq_edit_meta_keywords'] = 'Meta-keywords of the answer';

$il['faq_button_name'] = 'Save';
$il['faq_module_label_propertes1'] = 'Template file';
$il['faq_module_method_name1'] = 'Returns the list of categories';

// new
$il['faq_modul_metod_pub_form'] = 'Output form to send a question';
$il['faq_modul_metod_show_partitions'] = 'Show sections';
$il['faq_modul_param_new_question_email_subj'] = 'The subject of the message for the admin about the new question';
$il['faq_list_datetime'] = 'When';
$il['faq_list_partition'] = 'Section';
$il['faq_list_question'] = 'Question';
$il['faq_menu_partitions'] = 'Sections';
$il['faq_list_show_partition_questions'] = 'Show partition questions';
$il['faq_modul_show_counters'] = 'Show Question Count';
$il['faq_list_has_answer'] = 'Is there a response?';
$il['faq_date_block'] = 'Select by date';
$il['faq_limit_questions'] = 'Number of questions per page';
$il['faq_pages_questions'] =' Max. number of pages in the block ';
$il['faq_sortorder_questions'] = 'Sorting order';
$il['faq_sortorder_questions_default'] = 'By id order of adding';
$il['faq_sortorder_questions_desc'] = 'By id order of decreasing';
$il['faq_sortorder_questions_new_at_top'] = 'First new';
$il['faq_sortorder_questions_new_at_bottom'] = 'First old';
$il['pub_show_title'] = 'Print header title';
$il['pub_show_title_def'] = 'Default header';
$il['pub_show_meta_keywords'] = 'Print meta-keywords';
$il['pub_show_meta_keywords_def'] = 'Meta-keywords by default';
$il['pub_show_meta_description'] = 'Print meta-description';
$il['pub_show_meta_description_def'] = 'Meta-description by default';


?>