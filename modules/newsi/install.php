<?php

/**
 * Модуль "Новости"
 *
 * @author Александр Ильин mecommayou@gmail.com
 * @copyright ArtProm (с) 2001-2008
 * @version 1.0 beta
 *
 */
class newsi_install extends install_modules
{
    /**
     * Инсталяция базового модуля
     *
     * @param string $id_module Идентификатор создаваемого базового модуля
     * @param boolean $reinstall переинсталяция?
     */
    function install($id_module, $reinstall = false)
    {
        global $kernel;

        $query = 'CREATE TABLE IF NOT EXISTS `'.PREFIX.'_newsi_cats` ( '
            . ' `id` int(10) unsigned NOT NULL auto_increment, '
            . ' `module_id` varchar(255) NOT NULL, '
            . ' `name` varchar(255) NOT NULL, '
            . ' `description` text default NULL, '
            . ' `parent_id` int(10) NOT NULL, '
            . ' PRIMARY KEY (`id`), '
            . ' KEY `module_id` (`module_id`), '
            . ' KEY `parent_id` (`parent_id`), '
            . ' UNIQUE KEY `name` (`name`) '
            . ' ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        $kernel->runSQL($query);

        $query = 'CREATE TABLE IF NOT EXISTS `'.PREFIX.'_newsi_tags` ( '
            . ' `id` int(10) unsigned NOT NULL auto_increment, '
            . ' `module_id` varchar(255) NOT NULL, '
            . ' `name` varchar(255) NOT NULL, '
            . ' `description` text default NULL, '
            . ' PRIMARY KEY (`id`), '
            . ' KEY `module_id` (`module_id`), '
            . ' UNIQUE KEY `name` (`name`) '
            . ' ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        $kernel->runSQL($query);

        $query = 'CREATE TABLE IF NOT EXISTS `'.PREFIX.'_newsi_taxonomy` ( '
            . ' `id` int(10) unsigned NOT NULL auto_increment, '
            . ' `module_id` varchar(255) NOT NULL, '
            . ' `pub_id` int(10) NOT NULL, '
            . ' `tag_id` int(10) NOT NULL, '
            . ' PRIMARY KEY (`id`), '
            . ' KEY `module_id` (`module_id`), '
            . ' KEY `pub_id` (`pub_id`), '
            . ' KEY `tag_id` (`tag_id`) '
            . ' ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        $kernel->runSQL($query);

        $query = 'CREATE TABLE IF NOT EXISTS `'.PREFIX.'_newsi_fields` ( '
            . ' `id` int(10) unsigned NOT NULL AUTO_INCREMENT, '
            . ' `module_id` varchar(255) NOT NULL, '
            . ' `field_type` enum(\'select\',\'string\',\'pagesite\',\'textarea\',\'checkbox\',\'date\',\'fileselect\',\'imageselect\') NOT NULL, '
            . ' `field_title` varchar(255) NOT NULL, '
            . ' `field_name` varchar(255) NOT NULL, '
            . ' `field_order` int(5) unsigned NOT NULL, '
            . ' `field_params` text, '
            . ' PRIMARY KEY (`id`), '
            . ' KEY `moduleid_order` (`module_id`,`field_order`) '
            . ' ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        $kernel->runSQL($query);

    }

    /**
     * Деинсталяция базового модуля
     *
     * @param string $id_module Идентификатор удаляемого базового модуля
     */
    function uninstall($id_module)
    {
        global $kernel;

        $query = 'DROP TABLE IF EXISTS `'.PREFIX.'_newsi`';
        $kernel->runSQL($query);

        $query = 'DROP TABLE `'.PREFIX.'_newsi_cats`';
        $kernel->runSQL($query);

        $query = 'DROP TABLE `'.PREFIX.'_newsi_tags`';
        $kernel->runSQL($query);

        $query = 'DROP TABLE `'.PREFIX.'_newsi_taxonomy`';
        $kernel->runSQL($query);

        $query = 'DROP TABLE `'.PREFIX.'_newsi_fields`';
        $kernel->runSQL($query);
    }

    /**
     * Инсталяция дочернего модуля
     *
     * @param string $id_module Идентификатор вновь создаваемого дочернего модуля
     * @param boolean $reinstall переинсталяция?
     */
    function install_children($id_module, $reinstall = false)
    {
        global $kernel;
        $kernel->pub_dir_create_in_images($id_module);
        $kernel->pub_dir_create_in_images($id_module.'/tn');
        $kernel->pub_dir_create_in_images($id_module.'/source');

        $query = 'CREATE TABLE IF NOT EXISTS `'.$kernel->pub_prefix_get().'_'.$id_module.'` ( '
            . ' `id` int(10) unsigned NOT NULL auto_increment, '
            . ' `cat_id` int(10) NOT NULL, '
            . ' `date` date NOT NULL, '
            . ' `time` time NOT NULL, '
            . ' `available` tinyint(1) unsigned NOT NULL, '
            . ' `lenta` tinyint(1) unsigned NOT NULL, '
            . ' `delivery` tinyint(1) unsigned NOT NULL, '
            . ' `rss` tinyint(1) unsigned NOT NULL, '
            . ' `header` varchar(255) NOT NULL, '
            . ' `description_short` text NOT NULL, '
            . ' `description_full` text NOT NULL, '
            . ' `html_title` text default NULL, '
            . ' `meta_keywords` text default NULL, '
            . ' `meta_description` text default NULL, '
            . ' `author` varchar(255) NOT NULL, '
            . ' `source_name` varchar(255) default NULL, '
            . ' `source_url` varchar(255) default NULL, '
            . ' `image` varchar(255) NOT NULL, '
            . ' `post_date` DATETIME default NULL, '
            . ' PRIMARY KEY  (`id`), '
            . ' KEY `cat_id` (`cat_id`), '
            . ' KEY `date` (`date`,`time`), '
            . ' KEY `available` (`available`), '
            . ' KEY `lenta` (`lenta`), '
            . ' KEY `delivery` (`delivery`), '
            . ' KEY `rss` (`rss`), '
            . ' KEY `header` (`header`), '
            . ' KEY `author` (`author`), '
            . ' KEY `post_date` (`post_date`), '
            . ' KEY `image` (`image`) '
            . ' ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        $kernel->runSQL($query);
    }

    /**
     * Деинсталяция дочернего модуля
     *
     *
     * @param string $id_module ID удоляемого дочернего модуля
     */
    function uninstall_children($id_module)
    {
        global $kernel;

        $kernel->pub_dir_recurs_delete('content/images/'.$id_module);

        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_gallery_cats` WHERE `module_id`="'.$id_module.'"';
        $kernel->runSQL($query);

        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_gallery_tags` WHERE `module_id`="'.$id_module.'"';
        $kernel->runSQL($query);

        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_newsi_taxonomy` WHERE `module_id`="'.$id_module.'"';
        $kernel->runSQL($query);

        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_newsi_fields` WHERE `module_id`="'.$id_module.'"';
        $kernel->runSQL($query);

        $query = 'DROP TABLE `'.$kernel->pub_prefix_get().'_'.$id_module.'`';
        $kernel->runSQL($query);

    }
}

$install = new newsi_install();

$install->set_name('[#news_base_name#]');
$install->set_id_modul('newsi');
$install->set_admin_interface(2);

// Ширина исходной картинки в пикселях
$property = new properties_string();
$property->set_caption('[#news_property_img_source_width#]');
$property->set_default('800');
$property->set_id('img_source_width');
$install->add_modul_properties($property);

// Высота исходной картинки в пикселях
$property = new properties_string();
$property->set_caption('[#news_property_img_source_height#]');
$property->set_default('800');
$property->set_id('img_source_height');
$install->add_modul_properties($property);

// Ширина большой картинки в пикселях
$property = new properties_string();
$property->set_caption('[#news_property_img_big_width#]');
$property->set_default('300');
$property->set_id('img_big_width');
$install->add_modul_properties($property);

// Высота большой картинки в пикселях
$property = new properties_string();
$property->set_caption('[#news_property_img_big_height#]');
$property->set_default('300');
$property->set_id('img_big_height');
$install->add_modul_properties($property);

// Ширина маленькой картинки в пикселях
$property = new properties_string();
$property->set_caption('[#news_property_img_small_width#]');
$property->set_default('100');
$property->set_id('img_small_width');
$install->add_modul_properties($property);

// Высота маленькой картинки в пикселях
$property = new properties_string();
$property->set_caption('[#news_property_img_small_height#]');
$property->set_default('100');
$property->set_id('img_small_height');
$install->add_modul_properties($property);



// Автоматически добавлять в рассылку
$property = new properties_checkbox();
$property->set_caption('[#news_property_watermark#]');
$property->set_default('false');
$property->set_id('watermark');
$install->add_modul_properties($property);

//фаил водяного знака
$property = new properties_file();
$property->set_id('path_to_copyright_file');
$property->set_caption('[#news_property_path_to_copyright_file#]');
$property->set_patch('modules/newsi/templates_user/watermark');
$property->set_mask('jpg,gif,png');
$property->set_default('');
$install->add_modul_properties($property);

//расположение водяного знака
$property = new properties_select();
$property->set_id('copyright_position');
$property->set_caption('[#news_property_copyright_position#]');
$property->set_data(array("0"=>"[#news_property_copyright_position_0#]",
    "1"=>"[#news_property_copyright_position_1#]",
    "2"=>"[#news_property_copyright_position_2#]",
    "3"=>"[#news_property_copyright_position_3#]",
    "4"=>"[#news_property_copyright_position_4#]"));
$property->set_default('4');
$install->add_modul_properties($property);

//прозрачность водяного знака
$property = new properties_select();
$property->set_caption('[#news_property_copyright_transparency#]');
$property->set_id('copyright_transparency');
$property->set_data(array("10"=>"10%",
    "20"=>"20%",
    "30"=>"30%",
    "40"=>"40%",
    "50"=>"50%",
    "60"=>"60%",
    "70"=>"70%",
    "80"=>"80%",
    "90"=>"90%",
    "100"=>"100%"));
$property->set_default('20');
$install->add_modul_properties($property);


// Автоматически добавлять в рассылку
$property = new properties_checkbox();
$property->set_caption('[#news_property_deliver#]');
$property->set_default('true');
$property->set_id('deliver');
$install->add_modul_properties($property);

// Автоматически добавлять в ленту
$property = new properties_checkbox();
$property->set_caption('[#news_property_lenta#]');
$property->set_default('true');
$property->set_id('lenta');
$install->add_modul_properties($property);

// Автоматически добавлять в rss
$property = new properties_checkbox();
$property->set_caption('[#news_property_rss#]');
$property->set_default('true');
$property->set_id('rss');
$install->add_modul_properties($property);

// Лимит новостей на страницу
$property = new properties_string();
$property->set_caption('[#news_property_news_per_page#]');
$property->set_default('10');
$property->set_id('news_per_page');
$install->add_modul_properties($property);

// Станица для просмотра полного текста новости
$property = new properties_pagesite();
$property->set_caption('[#news_property_page_for_lenta#]');
$property->set_default('');
$property->set_id('page_for_lenta');
$install->add_modul_properties($property);

// Формат вывода даты новости
$property = new properties_string();
$property->set_caption('[#news_property_news_date_format#]');
$property->set_default('d.m.Y');
$property->set_id('news_date_format');
$install->add_modul_properties($property);

// Использовать таксономии рубрик и категорий
$property = new properties_checkbox();
$property->set_caption('[#news_property_taxonomy#]');
$property->set_default('false');
$property->set_id('taxonomy');
$install->add_modul_properties($property);

// Идентификатор рубрик в URL
$property = new properties_string();
$property->set_caption('[#news_property_url_item_cats#]');
$property->set_default('category');
$property->set_id('url_item_cats');
$install->add_modul_properties($property);

// Идентификатор тегов в URL
$property = new properties_string();
$property->set_caption('[#news_property_url_item_tags#]');
$property->set_default('tags');
$property->set_id('url_item_tags');
$install->add_modul_properties($property);

// Публичный метод для отображения ленты новостей
$install->add_public_metod('pub_show_lenta', '[#news_pub_show_lenta#]');

// Шаблон ленты новостей
$property = new properties_file();
$property->set_caption('[#news_pub_show_lenta_template#]');
$property->set_default('modules/newsi/templates_user/lenta.html');
$property->set_id('template');
$property->set_mask('htm,html');
$property->set_patch('modules/newsi/templates_user');
$install->add_public_metod_parametrs('pub_show_lenta', $property);

// Заголовок новостной ленты
$property = new properties_string();
$property->set_caption('[#news_pub_show_lenta_header_list#]');
$property->set_default('');
$property->set_id('header_list');
$install->add_public_metod_parametrs('pub_show_lenta', $property);

// Количество новостей в ленте
$property = new properties_string();
$property->set_caption('[#news_pub_show_lenta_limit#]');
$property->set_default('10');
$property->set_id('limit');
$install->add_public_metod_parametrs('pub_show_lenta', $property);

// Тип отображения
$property = new properties_select();
$property->set_caption('[#news_pub_show_lenta_type#]');
$property->set_data(array(
    'default'   => '[#news_pub_show_lenta_type_default#]',
    'past'      => '[#news_pub_show_lenta_type_past#]',
    'future'    => '[#news_pub_show_lenta_type_future#]',
    'random'    => '[#news_pub_show_lenta_type_random#]'
));
$property->set_default('default');
$property->set_id('type');
$install->add_public_metod_parametrs('pub_show_lenta', $property);

// Станица для просмотра полного текста новости
$property = new properties_pagesite();
$property->set_caption('[#news_pub_show_lenta_page#]');
$property->set_default('index');
$property->set_id('page');
$install->add_public_metod_parametrs('pub_show_lenta', $property);

// ID модулей, для которых формируется данная новостаня лента
$property = new properties_string();
$property->set_caption('[#news_pub_show_lenta_id_modules#]');
$property->set_default('');
$property->set_id('id_modules');
$install->add_public_metod_parametrs('pub_show_lenta', $property);

// Публичный метод для отображения списка рубрик
$install->add_public_metod('pub_show_cats', '[#news_pub_show_cats#]');

// Шаблон списка рубрик
$property = new properties_file();
$property->set_caption('[#news_pub_show_cats_template#]');
$property->set_default('modules/newsi/templates_user/cats.html');
$property->set_id('cats_template');
$property->set_mask('htm,html');
$property->set_patch('modules/newsi/templates_user');
$install->add_public_metod_parametrs('pub_show_cats', $property);

// Заголовок списка рубрик
$property = new properties_string();
$property->set_caption('[#news_pub_show_cats_header#]');
$property->set_default('');
$property->set_id('cats_header');
$install->add_public_metod_parametrs('pub_show_cats', $property);

// Показывать пустые рубрики новостей
$property = new properties_checkbox();
$property->set_caption('[#news_pub_show_cats_ifempty#]');
$property->set_default('false');
$property->set_id('cats_ifempty');
$install->add_public_metod_parametrs('pub_show_cats', $property);

// Показывать счётчик записей в рубрике
$property = new properties_checkbox();
$property->set_caption('[#news_pub_show_ifcount#]');
$property->set_default('false');
$property->set_id('cats_ifcount');
$install->add_public_metod_parametrs('pub_show_cats', $property);

// Показывать список рубрик при чтении записи
$property = new properties_checkbox();
$property->set_caption('[#news_pub_show_infulltext#]');
$property->set_default('true');
$property->set_id('cats_infulltext');
$install->add_public_metod_parametrs('pub_show_cats', $property);

// Публичный метод для отображения списка тегов
$install->add_public_metod('pub_show_tags', '[#news_pub_show_tags#]');

// Шаблон списка тегов
$property = new properties_file();
$property->set_caption('[#news_pub_show_tags_template#]');
$property->set_default('modules/newsi/templates_user/tags.html');
$property->set_id('cats_template');
$property->set_mask('htm,html');
$property->set_patch('modules/newsi/templates_user');
$install->add_public_metod_parametrs('pub_show_tags', $property);

// Заголовок списка тегов
$property = new properties_string();
$property->set_caption('[#news_pub_show_tags_header#]');
$property->set_default('');
$property->set_id('tags_header');
$install->add_public_metod_parametrs('pub_show_tags', $property);

// Показывать счётчик записей с тегом
$property = new properties_checkbox();
$property->set_caption('[#news_pub_show_ifcount#]');
$property->set_default('false');
$property->set_id('tags_ifcount');
$install->add_public_metod_parametrs('pub_show_tags', $property);

// Показывать список тегов при чтении записи
$property = new properties_checkbox();
$property->set_caption('[#news_pub_show_infulltext#]');
$property->set_default('true');
$property->set_id('tags_infulltext');
$install->add_public_metod_parametrs('pub_show_tags', $property);


// Публичный метод для отображения архива новостей
$install->add_public_metod('pub_show_archive', '[#news_pub_show_archive#]');

// Шаблон архива
$property = new properties_file();
$property->set_caption('[#news_pub_show_archive_template#]');
$property->set_default('modules/newsi/templates_user/arhive.html');
$property->set_id('template');
$property->set_mask('htm,html');
$property->set_patch('modules/newsi/templates_user');
$install->add_public_metod_parametrs('pub_show_archive', $property);

// Количество новостей на страницу
$property = new properties_string();
$property->set_caption('[#news_pub_show_archive_limit#]');
$property->set_default('10');
$property->set_id('limit');
$install->add_public_metod_parametrs('pub_show_archive', $property);

// Тип отоба
$property = new properties_select();
$property->set_caption('[#news_pub_show_archive_type#]');
$property->set_data(array(
    'default'   => '[#news_pub_show_archive_default#]',
    'past'      => '[#news_pub_show_archive_past#]',
    'future'    => '[#news_pub_show_archive_future#]'
));
$property->set_default('default');
$property->set_id('type');
$install->add_public_metod_parametrs('pub_show_archive', $property);

//Тип вывода блока для постраничной навигации в архиве
$property = new properties_select();
$property->set_caption('[#news_pub_pages_type#]');
$property->set_data(array(
    'block' => '[#news_pub_pages_get_block#]',
    'float' => '[#news_pub_pages_get_float#]'
));
$property->set_default('block');
$property->set_id('pages_type');
$install->add_public_metod_parametrs('pub_show_archive', $property);

// Количество выводимых страниц
$property = new properties_string();
$property->set_caption('[#news_property_pages_count#]');
$property->set_default('5');
$property->set_id('pages_count');
$install->add_public_metod_parametrs('pub_show_archive', $property);


$install->add_public_metod('pub_show_selection', '[#news_pub_show_selection#]');
$property = new properties_file();
$property->set_caption('[#news_pub_show_selection_template#]');
$property->set_default('modules/newsi/templates_user/arhive.html');
$property->set_id('template');
$property->set_mask('htm,html');
$property->set_patch('modules/newsi/templates_user');
$install->add_public_metod_parametrs('pub_show_selection', $property);

// Публичный метод для отображения `title` новости
$install->add_public_metod('pub_show_html_title', '[#news_pub_show_html_title#]');
$property = new properties_string();
$property->set_caption('[#news_pub_pub_show_html_title_def#]');
$property->set_default('');
$property->set_id('html_title_def');
$install->add_public_metod_parametrs('pub_show_html_title', $property);

// Публичный метод для отображения `meta-keywords` новости
$install->add_public_metod('pub_show_meta_keywords', '[#news_pub_show_meta_keywords#]');
$property = new properties_string();
$property->set_caption('[#news_pub_pub_show_meta_keywords_def#]');
$property->set_default('');
$property->set_id('meta_keywords_def');
$install->add_public_metod_parametrs('pub_show_meta_keywords', $property);

// Публичный метод для отображения `meta-description` новости
$install->add_public_metod('pub_show_meta_description', '[#news_pub_show_meta_description#]');
$property = new properties_string();
$property->set_caption('[#news_pub_pub_show_meta_description_def#]');
$property->set_default('');
$property->set_id('meta_description_def');
$install->add_public_metod_parametrs('pub_show_meta_description', $property);


$install->module_copy[0]['name'] = 'newsi_modul_base_name1';

$install->module_copy[0]['action'][0]['caption']    = 'Вывести ленту';
$install->module_copy[0]['action'][0]['id_metod']   = 'pub_show_lenta';
$install->module_copy[0]['action'][0]['properties']['template'] = 'modules/newsi/templates_user/lenta.html';
$install->module_copy[0]['action'][0]['properties']['limit']    = '5';
$install->module_copy[0]['action'][0]['properties']['type']     = 'past';
$install->module_copy[0]['action'][0]['properties']['page']     = 'index';

$install->module_copy[0]['action'][1]['caption']    = 'Вывести архив';
$install->module_copy[0]['action'][1]['id_metod']   = 'pub_show_archive';
$install->module_copy[0]['action'][1]['properties']['template']    = 'modules/newsi/templates_user/arhive.html';
$install->module_copy[0]['action'][1]['properties']['limit']       = '20';
$install->module_copy[0]['action'][1]['properties']['type']        = 'past';
$install->module_copy[0]['action'][1]['properties']['pages_type']  = 'block';
$install->module_copy[0]['action'][1]['properties']['pages_count'] = '5';

